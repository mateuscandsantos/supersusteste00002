﻿using System;
using UnityEngine;

public static class GameController
{

    public static int NUM_BADGES = 21;

    public static bool INITIAL_SETTING_IS_SOUND_ENABLED = true;
    public static float INITIAL_SETTING_DIFFICULTY = 0.33f;
    public static float INITIAL_SETTING_SOUND_LEVEL = 0.5f;

    public static DBManager dbManager;
    public static PlayerLDB player = new PlayerLDB();
    public static SettingsLDB settings = new SettingsLDB();

    public static bool hasCompletedLevel1 = false;
    public static bool hasCompletedLevel2 = false;
    public static bool hasCompletedLevel3 = false;


    public static void LoadGame()
    {
        dbManager = new DBManager();

        LoadPlayer();
        LoadSettings();
        VerifyHasCompletedLevel();
        ApplyGlobalSoundSettings();
    }

    private static void LoadPlayer()
    {
        player = dbManager.GetPlayer();

        if (player == null)
        {
            dbManager.SavePlayer(new PlayerLDB());
            player = dbManager.GetPlayer();
        }
    }

    private static void LoadSettings()
    {
        settings = dbManager.GetSettings();

        if (settings == null)
        {
            dbManager.SaveSettings(new SettingsLDB());
            settings = dbManager.GetSettings();
        }
    }

    public static void UpdatePlayer()
    {
        dbManager.SavePlayer(player);
    }

    public static void UpdateSettings()
    {
        dbManager.SaveSettings(settings);
    }

    public static void UpdateStatusMiniGameN(int miniGameNumber, bool status)
    {

        if (player != null)
        {
            player.SetHasBadge(miniGameNumber, status);
            UpdatePlayer();
            VerifyHasCompletedLevel();

            if (status) {
                LevelController.FINISHED_GAME_NUMBER = miniGameNumber;
            }
        }
    }

    public static void VerifyHasCompletedLevel()
    {

        if (player != null)
        {
            if (player.HasBadge(1) && player.HasBadge(2) && player.HasBadge(3) &&
            player.HasBadge(4) && player.HasBadge(5) && player.HasBadge(6) && player.HasBadge(7))
            {
                hasCompletedLevel1 = true;
            }

            if (player.HasBadge(8) && player.HasBadge(9) && player.HasBadge(10) &&
                player.HasBadge(11) && player.HasBadge(12) && player.HasBadge(13) && player.HasBadge(14))
            {
                hasCompletedLevel2 = true;
            }

            if (player.HasBadge(15) && player.HasBadge(16) && player.HasBadge(17) &&
                player.HasBadge(18) && player.HasBadge(19) && player.HasBadge(20) && player.HasBadge(21))
            {
                hasCompletedLevel3 = true;
            }

            if (hasCompletedLevel3)
            {
                player.SetLevel(3);
            }
            else if (hasCompletedLevel2)
            {
                player.SetLevel(2);
            }
            else if (hasCompletedLevel1)
            {
                player.SetLevel(1);
            }
            else
            {
                player.SetLevel(0);
            }

            UpdatePlayer();
        }
    }

    public static void ApplyGlobalSoundSettings() {
        FMODUnity.RuntimeManager.MuteAllEvents(!settings.IsSoundEnabled());
        SoundController.InitSound();
    }

    public static void UpdateCurrentX(float currentX)
    {
        if (player != null) {
            player.SetCurrentX(currentX);
            UpdatePlayer();
        }
    }

}