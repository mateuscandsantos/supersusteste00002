﻿using System;
using UnityEngine;
using Anima2D;

public class GlobalPlayerController : MonoBehaviour
{

    // Movement management
    public bool AUTOMATICALLY_MOVE_FORWARD = false;
    public float INITIAL_HORIZONTAL_ACCELERATION_RATIO = 0.01f;
    public float MAX_HORIZONTAL_ACCELERATION_RATIO = 0.5f;
    public float MAX_HORIZONTAL_DELTA = 1f;
    public float MAX_VERTICAL_DELTA = 1f;
    public float JUMP_FORCE = 10f;

    public static bool isMovementLocked = false;

    public LayerMask groundLayers;
    public float horizontalAccelerationRatio = 0.01f;
    public bool facingRight = true;
    public bool isInDynamicScene = false;

    private Rigidbody2D mPlayerRb;
    private CapsuleCollider2D mPlayerFeetCollider;
    private Animator mAnim;
    private float deltaX = 0f;
    private float deltaY = 0f;

    private bool hasInitialized = false;

    // Appearance management
    public SpriteMeshAnimation hairAnimation;
    public SpriteMeshAnimation eyesAnimation;
    public SpriteMeshAnimation headAnimation;
    public SpriteMeshAnimation leftUpperArmAnimation;
    public SpriteMeshAnimation leftLowerArmAnimation;
    public SpriteMeshAnimation rightUpperArmAnimation;
    public SpriteMeshAnimation rightLowerArmAnimation;
    public SpriteMeshAnimation torsoAnimation;
    public SpriteMeshAnimation leftUpperLegAnimation;
    public SpriteMeshAnimation leftLowerLegAnimation;
    public SpriteMeshAnimation rightUpperLegAnimation;
    public SpriteMeshAnimation rightLowerLegAnimation;

    private int mSelectedHead = 0;
    private int mSelectedHair = 0;
    private int mSelectedEyes = 0;
    private int mSelectedLeftUpperArm = 0;
    private int mSelectedLeftLowerArm = 0;
    private int mSelectedRightUpperArm = 0;
    private int mSelectedRightLowerArm = 0;
    private int mSelectedTorso = 0;
    private int mSelectedLeftUpperLeg = 0;
    private int mSelectedLeftLowerLeg = 0;
    private int mSelectedRightUpperLeg = 0;
    private int mSelectedRightLowerLeg = 0;

    #region MovementInitialization
    void Start()
    {
        // Movement initialization
        InitScene();
        InitWalkingAnimation();

        // Appearance initialization
        LoadUserAppearance();
        UpdateAppearance();
    }

    private void InitScene()
    {
        mAnim = GetComponent<Animator>();

        if (isInDynamicScene)
        {
            mPlayerRb = GetComponent<Rigidbody2D>();
            mPlayerFeetCollider = GetComponent<CapsuleCollider2D>();

            JoystickController.horizontalDir = 0;
            JoystickController.verticalDir = 0;
        }
    }

    public void InitWalkingAnimation()
    {

        if (!isInDynamicScene && mAnim != null)
        {
            mAnim.Play("MainCharacterWalkingAnim");
        }
    }
    #endregion

    #region AppearanceInitialization
    public void LoadUserAppearance()
    {
        mSelectedHead = 0;
        mSelectedHair = GameController.player.GetHair();
        mSelectedEyes = GameController.player.GetEyes();
        mSelectedHead = GameController.player.GetHead();

        mSelectedLeftUpperArm = GameController.player.GetLeftUpperArm();
        mSelectedLeftLowerArm = GameController.player.GetLeftLowerArm();
        mSelectedRightUpperArm = GameController.player.GetRightUpperArm();
        mSelectedRightLowerArm = GameController.player.GetRightLowerArm();

        mSelectedTorso = GameController.player.GetTorso();

        mSelectedLeftUpperLeg = GameController.player.GetLeftUpperLeg();
        mSelectedLeftLowerLeg = GameController.player.GetLeftLowerLeg();
        mSelectedRightUpperLeg = GameController.player.GetRightUpperLeg();
        mSelectedRightLowerLeg = GameController.player.GetRightLowerLeg();
    }
    #endregion

    #region MovementUpdates
    void LateUpdate()
    {

        if (isInDynamicScene) 
        {
            TryInitializeStoredXPosition();
            FlipHorizontalDirection();
            UpdateHorizontalPosition();
            UpdateVerticalPosition();
        }
    }

    void TryInitializeStoredXPosition() {

        if (LevelController.FINISHED_GAME_NUMBER > 0 && !hasInitialized)
        {
            hasInitialized = true;
            transform.position = new Vector3(GameController.player.GetCurrentX() + 3,
                                             transform.position.y,
                                             transform.position.z);
        }
    }

    void UpdateHorizontalPosition() {
        transform.position = new Vector3(transform.position.x + (GetDeltaX() * Time.deltaTime), 
                                         transform.position.y, 
                                         transform.position.z);
    }

    float GetDeltaX()
    {

        if (AUTOMATICALLY_MOVE_FORWARD) {
            JoystickController.horizontalDir = 1;
        }

        // Get the status of the joystick directional button, apply a reductor 
        // to the value coming from the joystick and apply the current 
        // acceleration ratio
        deltaX = (JoystickController.horizontalDir * 0.2f) * horizontalAccelerationRatio;

        // Limit the horizontal delta of the next move
        if (deltaX > MAX_HORIZONTAL_DELTA)
        {
            deltaX = MAX_HORIZONTAL_DELTA;
        }

        else if (deltaX < -MAX_HORIZONTAL_DELTA)
        {
            deltaX = -MAX_HORIZONTAL_DELTA;
        }

        // Increase the player acceleration forwards or backwards or resets 
        // the acceleration if the user stop pressing the left or right buttons 
        if (deltaX > 0 || deltaX < 0)
        {

            if (horizontalAccelerationRatio > MAX_HORIZONTAL_ACCELERATION_RATIO)
            {
                horizontalAccelerationRatio = MAX_HORIZONTAL_ACCELERATION_RATIO;
            }
            else
            {
                horizontalAccelerationRatio += INITIAL_HORIZONTAL_ACCELERATION_RATIO;
            }
        }
        else
        {
            horizontalAccelerationRatio = INITIAL_HORIZONTAL_ACCELERATION_RATIO;
        }

        if (!facingRight && JoystickController.horizontalDir > 0)
        {
            deltaX = -3.5f;
        }
        else if (facingRight && JoystickController.horizontalDir < 0)
        {
            deltaX = 3.5f;
        }

        // Update the horizontal animation of the player
        if (!isMovementLocked && mAnim != null)
        {
            mAnim.SetFloat("HorSpeed", Math.Abs(deltaX * 100));
        }

        return deltaX * 50;
    }

    void FlipHorizontalDirection()
    {

        if (!facingRight && JoystickController.horizontalDir > 0)
        {
            facingRight = true;
            transform.eulerAngles = new Vector2(0, 0);
        }
        else if (facingRight && JoystickController.horizontalDir < 0)
        {
            facingRight = false;
            transform.eulerAngles = new Vector2(0, 180);
        }
    }

    void UpdateVerticalPosition()
    {
        // Get the status of the joystick directional button, apply a reductor 
        // to the value coming from the joystick and apply the current 
        // acceleration ratio
        deltaY = transform.position.y - deltaY;

        // Limit the vertical delta of the next move
        if (deltaY > MAX_VERTICAL_DELTA)
        {
            deltaY = MAX_VERTICAL_DELTA;

        }
        else if (deltaY < -MAX_VERTICAL_DELTA)
        {
            deltaY = -MAX_VERTICAL_DELTA;
        }

        // Update the animation of the character
        if (!isMovementLocked)
        {

            if (mAnim != null) {
                mAnim.SetFloat("VerSpeed", deltaY);
            }

            if (IsPlayerGrounded() && JoystickController.verticalDir > 0)
            {
                mPlayerRb.AddForce(Vector2.up * JUMP_FORCE, ForceMode2D.Impulse);
            }
        }
    }

    bool IsPlayerGrounded()
    {

        if (mPlayerFeetCollider != null)
        {
            Vector2 startPos = new Vector2(
            mPlayerFeetCollider.transform.position.x,
            mPlayerFeetCollider.transform.position.y + 1f);

            Vector2 endPos = Vector2.down;

            return Physics2D.Raycast(startPos, endPos, 5f, groundLayers);
        }
        else
        {
            return false;
        }
    }
    #endregion

    #region Events
    public void OnTriggerEnter2D(Collider2D collid)
    {
        DetectCollisionWithMiniGame(collid);
    }

    void DetectCollisionWithMiniGame(Collider2D collid)
    {
        // Check collisions with the objects that make the player enter into a 
        // migi-game
        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_1)
        {
            MenuController.SOpenMiniGameNIntro(1);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_2)
        {
            MenuController.SOpenMiniGameNIntro(2);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_3)
        {
            MenuController.SOpenMiniGameNIntro(3);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_4)
        {
            MenuController.SOpenMiniGameNIntro(4);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_5)
        {
            MenuController.SOpenMiniGameNIntro(5);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_6)
        {
            MenuController.SOpenMiniGameNIntro(6);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_7)
        {
            MenuController.SOpenMiniGameNIntro(7);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_8)
        {
            MenuController.SOpenMiniGameNIntro(8);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_9)
        {
            MenuController.SOpenMiniGameNIntro(9);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_10)
        {
            MenuController.SOpenMiniGameNIntro(10);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_11)
        {
            MenuController.SOpenMiniGameNIntro(11);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_12)
        {
            MenuController.SOpenMiniGameNIntro(12);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_13)
        {
            MenuController.SOpenMiniGameNIntro(13);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_14)
        {
            MenuController.SOpenMiniGameNIntro(14);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_15)
        {
            MenuController.SOpenMiniGameNIntro(15);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_16)
        {
            MenuController.SOpenMiniGameNIntro(16);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_17)
        {
            MenuController.SOpenMiniGameNIntro(17);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_18)
        {
            MenuController.SOpenMiniGameNIntro(18);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_19)
        {
            MenuController.SOpenMiniGameNIntro(19);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_20)
        {
            MenuController.SOpenMiniGameNIntro(20);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_MINI_GAME_21)
        {
            MenuController.SOpenMiniGameNIntro(21);
        }

        // Check collisions with the objects that make the transition from one 
        // level to another
        if (collid.gameObject.tag == CollidablesUtils.TAG_CHECK_LEVEL_1)
        {
            MenuController.SOpenLevelNCompleted(1);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_CHECK_LEVEL_2 && GameController.hasCompletedLevel2)
        {
            MenuController.SOpenLevelNCompleted(2);
        }

        if (collid.gameObject.tag == CollidablesUtils.TAG_CHECK_LEVEL_3 && GameController.hasCompletedLevel3)
        {
            MenuController.SOpenLevelNCompleted(3);
        }
    }
    #endregion

    #region AppearanceSelection
    public void SelectHair(bool left)
    {

        if (left)
        {

            if (MathUtils.IsEqual(mSelectedHair, 0))
            {
                mSelectedHair = 6;
            }
            else
            {
                mSelectedHair -= 1;
            }
        }
        else
        {

            if (MathUtils.IsEqual(mSelectedHair, 6))
            {
                mSelectedHair = 0;
            }
            else
            {
                mSelectedHair += 1;
            }
        }

        UpdateAppearance();
    }

    public void SelectEyes(bool left)
    {

        if (left)
        {

            if (MathUtils.IsEqual(mSelectedEyes, 0))
            {
                mSelectedEyes = 2;
            }
            else
            {
                mSelectedEyes -= 1;
            }
        }
        else
        {

            if (MathUtils.IsEqual(mSelectedEyes, 2))
            {
                mSelectedEyes = 0;
            }
            else
            {
                mSelectedEyes += 1;
            }
        }

        UpdateAppearance();
    }

    public void SelectTShirt(bool left)
    {

        if (left)
        {

            if (MathUtils.IsEqual(mSelectedTorso, 0))
            {
                mSelectedTorso = 3;
                mSelectedLeftUpperArm = 3;
                mSelectedLeftLowerArm = 3;
                mSelectedRightUpperArm = 3;
                mSelectedRightLowerArm = 3;
            }
            else
            {
                mSelectedTorso -= 1;
                mSelectedLeftUpperArm -= 1;
                mSelectedLeftLowerArm -= 1;
                mSelectedRightUpperArm -= 1;
                mSelectedRightLowerArm -= 1;
            }
        }
        else
        {

            if (MathUtils.IsEqual(mSelectedTorso, 3))
            {
                mSelectedTorso = 0;
                mSelectedLeftUpperArm = 0;
                mSelectedLeftLowerArm = 0;
                mSelectedRightUpperArm = 0;
                mSelectedRightLowerArm = 0;
            }
            else
            {
                mSelectedTorso += 1;
                mSelectedLeftUpperArm += 1;
                mSelectedLeftLowerArm += 1;
                mSelectedRightUpperArm += 1;
                mSelectedRightLowerArm += 1;
            }
        }

        UpdateAppearance();
    }

    public void SelectPants(bool left)
    {

        if (left)
        {

            if (MathUtils.IsEqual(mSelectedLeftUpperLeg, 0))
            {
                mSelectedLeftUpperLeg = 4;
                mSelectedLeftLowerLeg = 4;
                mSelectedRightUpperLeg = 4;
                mSelectedRightLowerLeg = 4;
            }
            else
            {
                mSelectedLeftUpperLeg -= 1;
                mSelectedLeftLowerLeg -= 1;
                mSelectedRightUpperLeg -= 1;
                mSelectedRightLowerLeg -= 1;
            }
        }
        else
        {

            if (MathUtils.IsEqual(mSelectedLeftUpperLeg, 4))
            {
                mSelectedLeftUpperLeg = 0;
                mSelectedLeftLowerLeg = 0;
                mSelectedRightUpperLeg = 0;
                mSelectedRightLowerLeg = 0;
            }
            else
            {
                mSelectedLeftUpperLeg += 1;
                mSelectedLeftLowerLeg += 1;
                mSelectedRightUpperLeg += 1;
                mSelectedRightLowerLeg += 1;
            }
        }

        UpdateAppearance();
    }

    public void UpdateAppearance()
    {

        if (mSelectedHair >= 0 && mSelectedHair < hairAnimation.frames.Length)
        {
            hairAnimation.frame = mSelectedHair;
        }

        if (mSelectedEyes >= 0 && mSelectedEyes < eyesAnimation.frames.Length)
        {
            eyesAnimation.frame = mSelectedEyes;
        }

        if (mSelectedHead >= 0 && mSelectedHead < headAnimation.frames.Length)
        {
            headAnimation.frame = mSelectedHead;
        }

        if (mSelectedTorso >= 0 && mSelectedTorso < torsoAnimation.frames.Length)
        {
            torsoAnimation.frame = mSelectedTorso;
        }

        if (mSelectedLeftUpperArm >= 0 && mSelectedLeftUpperArm < leftUpperArmAnimation.frames.Length)
        {
            leftUpperArmAnimation.frame = mSelectedLeftUpperArm;
        }

        if (mSelectedLeftLowerArm >= 0 && mSelectedLeftLowerArm < leftLowerArmAnimation.frames.Length)
        {
            leftLowerArmAnimation.frame = mSelectedLeftLowerArm;
        }

        if (mSelectedRightUpperArm >= 0 && mSelectedRightUpperArm < rightUpperArmAnimation.frames.Length)
        {
            rightUpperArmAnimation.frame = mSelectedRightUpperArm;
        }

        if (mSelectedRightLowerArm >= 0 && mSelectedRightLowerArm < rightLowerArmAnimation.frames.Length)
        {
            rightLowerArmAnimation.frame = mSelectedRightLowerArm;
        }

        if (mSelectedLeftUpperLeg >= 0 && mSelectedLeftUpperLeg < leftUpperLegAnimation.frames.Length)
        {
            leftUpperLegAnimation.frame = mSelectedLeftUpperLeg;
        }

        if (mSelectedLeftLowerLeg >= 0 && mSelectedLeftLowerLeg < leftLowerLegAnimation.frames.Length)
        {
            leftLowerLegAnimation.frame = mSelectedLeftLowerLeg;
        }

        if (mSelectedRightUpperLeg >= 0 && mSelectedRightUpperLeg < rightUpperLegAnimation.frames.Length)
        {
            rightUpperLegAnimation.frame = mSelectedRightUpperLeg;
        }

        if (mSelectedRightLowerLeg >= 0 && mSelectedRightLowerLeg < rightLowerLegAnimation.frames.Length)
        {
            rightLowerLegAnimation.frame = mSelectedRightLowerLeg;
        }
    }
    #endregion

    #region AppearancePersistency
    public void SaveAppearance(string playerName, string playerAge)
    {
        GameController.player.SetName(playerName);
        GameController.player.SetAge(int.Parse(playerAge));

        GameController.player.SetHair(mSelectedHair);
        GameController.player.SetEyes(mSelectedEyes);
        GameController.player.SetHead(mSelectedHead);

        GameController.player.SetLeftUpperArm(mSelectedLeftUpperArm);
        GameController.player.SetLeftLowerArm(mSelectedLeftLowerArm);
        GameController.player.SetRightUpperArm(mSelectedRightUpperArm);
        GameController.player.SetRightLowerArm(mSelectedRightLowerArm);

        GameController.player.SetTorso(mSelectedTorso);

        GameController.player.SetLeftUpperLeg(mSelectedLeftUpperLeg);
        GameController.player.SetLeftLowerLeg(mSelectedLeftLowerLeg);
        GameController.player.SetRightUpperLeg(mSelectedRightUpperLeg);
        GameController.player.SetRightLowerLeg(mSelectedRightLowerLeg);

        GameController.UpdatePlayer();
    }
    #endregion
}