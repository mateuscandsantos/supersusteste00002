﻿using UnityEngine;

public class JoystickController : MonoBehaviour {

    public static float horizontalDir = 0f;
    public static float verticalDir = 0f;

    public void LeftStart() {
        horizontalDir = -1;
    }

    public void LeftEnd() {
        horizontalDir = 0;
    }

    public void RightStart() {
        horizontalDir = 1;
    }

    public void RightEnd() {
        horizontalDir = 0;
    }

    public void UpStart() {
        verticalDir = 1;
    }

    public void UpStop() {
        verticalDir = 0;
    }

}