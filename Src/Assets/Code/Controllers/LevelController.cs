﻿using UnityEngine;

public class LevelController : MonoBehaviour 
{

    public int LEVEL_NUMBER = 1;
    public static int FINISHED_GAME_NUMBER = 0;

    public float MINI_GAME_1_SELECTOR = -2.1f;
    public float MINI_GAME_2_SELECTOR = 73;
    public float MINI_GAME_3_SELECTOR = 122;
    public float MINI_GAME_4_SELECTOR = 188f;
    public float MINI_GAME_5_SELECTOR = 244f;
    public float MINI_GAME_6_SELECTOR = 290f;
    public float MINI_GAME_7_SELECTOR = 337f;

    void Start()
    {
        GameController.LoadGame();
        GlobalPlayerController.isMovementLocked = false;
        SetCurrentPlayerXPosition();
    }

    void SetCurrentPlayerXPosition() 
    {

        switch (FINISHED_GAME_NUMBER)
        {

            case 0: 
                {
                    GameController.player.SetCurrentX(MINI_GAME_1_SELECTOR);
                    break;
                }

            case 1:
                {
                    GameController.player.SetCurrentX(MINI_GAME_1_SELECTOR + 14);
                    break;
                }

            case 2:
                {
                    GameController.player.SetCurrentX(MINI_GAME_2_SELECTOR);
                    break;
                }

            case 3:
                {
                    GameController.player.SetCurrentX(MINI_GAME_3_SELECTOR);
                    break;
                }

            case 4:
                {
                    GameController.player.SetCurrentX(MINI_GAME_4_SELECTOR);
                    break;
                }

            case 5:
                {
                    GameController.player.SetCurrentX(MINI_GAME_5_SELECTOR);
                    break;
                }

            case 6:
                {
                    GameController.player.SetCurrentX(MINI_GAME_6_SELECTOR);
                    break;
                }

            case 7:
                {
                    GameController.player.SetCurrentX(MINI_GAME_7_SELECTOR);
                    break;
                }
        }
    }

}