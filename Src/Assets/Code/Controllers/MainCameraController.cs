﻿using UnityEngine;

public class MainCameraController : MonoBehaviour 
{
    
    public Transform target;
    public Vector3 velocity = Vector3.zero;

    public float smoothTime = 0.15f;
    public float offsetRightSideXValue = 0f;

    public bool enableClampXValue = true;
    public float minXValue = 0;
    public float maxXValue = 0;

    public bool enableClampYValue = true;
    public float minYValue = 0;
    public float maxYValue = 0;

    void LateUpdate() {
        // Get the current position of the camera
        Vector3 targetPosition = target.position;

        // Align the camera and the target's position, clamping the X and Y axes
        // of the camera if this feature was enabled.
        if (enableClampXValue) {
            targetPosition.x = Mathf.Clamp(target.position.x, minXValue, maxXValue) + offsetRightSideXValue;
        } else {
            targetPosition.x = target.position.x + offsetRightSideXValue;
        }

        if (enableClampYValue) {
            targetPosition.y = Mathf.Clamp(target.position.y, minYValue, maxYValue);
        } else {
            targetPosition.y = target.position.y;
        }

        targetPosition.z = transform.position.z;

        // Update the position of the camera.
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
    }

}