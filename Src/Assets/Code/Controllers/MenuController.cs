﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour 
{

	public static readonly string SCENE_INTRO = "Intro";
    public static readonly string SCENE_SPLASH = "Splash";
	public static readonly string SCENE_PROFILE = "Profile";
	public static readonly string SCENE_SETTINGS = "Settings";
	public static readonly string SCENE_SHARE = "Share";
	public static readonly string SCENE_ABOUT = "About";
	public static readonly string SCENE_LETTER = "Letter";
	public static readonly string SCENE_ACHIEVEMENTS = "Achievements";
	public static readonly string SCENE_GAME_LEVELS = "GameLevels";
    public static readonly string SCENE_GAME_LEVEL_PREFIX = "Level";
    public static readonly string SCENE_GAME_OVER = "Game1_Over";

    /**
     * Navigation methods
     */
    public static void SOpenSplash() {
        SceneManager.LoadScene(MenuController.SCENE_SPLASH);
    }
    
    public static void SOpenIntro() {
        SceneManager.LoadScene(MenuController.SCENE_INTRO);
    }

    public static void SOpenProfile() {
        SceneManager.LoadScene(MenuController.SCENE_PROFILE);
    }

    public static void SOpenSettings() {
        SceneManager.LoadScene(MenuController.SCENE_SETTINGS);
    }

    public static void SOpenShare() {
        SceneManager.LoadScene(MenuController.SCENE_SHARE);
    }

    public static void SOpenAbout() {
        SceneManager.LoadScene(MenuController.SCENE_ABOUT);
    }

    public static void SOpenLetter() {
        SceneManager.LoadScene(MenuController.SCENE_LETTER);
    }

    public static void SOpenAchievements() {
        SceneManager.LoadScene(MenuController.SCENE_ACHIEVEMENTS);
    }

    public static void SOpenLevels() {
        SceneManager.LoadScene(MenuController.SCENE_GAME_LEVELS);
    }

    public static void SOpenLevel(int level)
    {

        if (GameController.player.GetLevel() >= level - 1)
        {
            SceneManager.LoadScene(MenuController.SCENE_GAME_LEVEL_PREFIX + level);
        }
        else
        {
            SOpenLevels();
        }
    }

    public static void SOpenLevelNCompleted(int level)
    {
        SceneManager.LoadScene(MenuController.SCENE_GAME_LEVEL_PREFIX + level + "_Completed");
    }

    public static void SOpenGameOver() {
        SceneManager.LoadScene(MenuController.SCENE_GAME_OVER);
    }

    public static void SOpenMiniGameNIntro(int miniGame)
    {
        bool passedLevel1 = miniGame < 8;
        bool passedLevel2 = miniGame > 7 && miniGame < 15 && GameController.hasCompletedLevel1;
        bool passedLevel3 = miniGame > 14 && miniGame < 22 && GameController.hasCompletedLevel1 && GameController.hasCompletedLevel2;

        if (passedLevel1 || passedLevel2 || passedLevel3)
        {

            if (passedLevel3 && GameController.player.hasPreviousBadges(miniGame))
            {
                SceneManager.LoadScene("MiniGame" + miniGame.ToString() + "_Intro");
            }
            else if (passedLevel2 && GameController.player.hasPreviousBadges(miniGame))
            {
                SceneManager.LoadScene("MiniGame" + miniGame.ToString() + "_Intro");
            }
            else if (passedLevel1 && GameController.player.hasPreviousBadges(miniGame))
            {
                SceneManager.LoadScene("MiniGame" + miniGame.ToString() + "_Intro");
            }
        }
    }

    public static void SOpenMiniGameN(int miniGame)
    {
        SceneManager.LoadScene("MiniGame" + miniGame.ToString() + "_Game");
    }

    public static void SOpenMiniGameNEnding(int miniGame, bool success)
    {

        if (success)
        {
            SceneManager.LoadScene("MiniGame" + miniGame.ToString() + "_Completed");
        }
        else
        {
            SceneManager.LoadScene("MiniGame" + miniGame.ToString() + "_Failure");
        }
    }

    /**
     * Instance methods that allow binding this class as a MonoBehavior to a 
     * component on the UI Editor.
     */
    public void OpenIntro() {
        MenuController.SOpenIntro();
    }

    public void OpenProfile() {
        MenuController.SOpenProfile();
    }

    public void OpenSettings() {
        MenuController.SOpenSettings();
    }

    public void OpenShare() {
        MenuController.SOpenShare();
    }

    public void OpenAbout() {
        MenuController.SOpenAbout();
    }

    public void OpenLetter() {
        MenuController.SOpenLetter();
    }

    public void OpenAchievements() {
        MenuController.SOpenAchievements();
    }

    public void ExitMiniGameToLevels()
    {
        SoundController.StopSoundsMiniGames();
        MenuController.SOpenLevels();
    }

    public void OpenLevels() {
        MenuController.SOpenLevels();
    }

    public void OpenLevelN(int level) {
        MenuController.SOpenLevel(level);
    }

    public void OpenGameOver() {
        MenuController.SOpenLevels();
    }

    public void OpenMiniGameNIntro(int miniGame)
    {
        Debug.Log("Mini game: " + miniGame);
        MenuController.SOpenMiniGameNIntro(miniGame);
    }

}