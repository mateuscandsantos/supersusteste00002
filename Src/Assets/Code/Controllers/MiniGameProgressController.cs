﻿using System;
using System.Diagnostics;
using UnityEngine;

public class MiniGameProgressController : ExecuteAfterNSecsBehavior
{

    public int MINI_GAME_NUMBER;
    public float TOTAL_TIME_MILLI = 6000;

    public static float totalTimeMilli = 6000;
    public static bool isMiniGamePaused = false;
    public static bool isShowingMiniGameEnding = false;

    public GameObject progressBarTop;
    public GameObject progressBarBottom;
    public GameObject progressBar;
    public GameObject btnPlay;
    public GameObject btnPause;
    public GameObject btnExitToMainMenu;
    public static int miniGameStatus = 0;  // -1 lose | 0 in progress | 1 win
    public static int percentRemainingTime = 0;

    private float remainingTimeMilli = -1;
    private int previousPercentRemainingTime = 100;

    private Stopwatch swProgressBar;

    void Start()
    {
        totalTimeMilli = TOTAL_TIME_MILLI;
        remainingTimeMilli = TOTAL_TIME_MILLI;
        swProgressBar = new Stopwatch();
        swProgressBar.Start();
        btnExitToMainMenu.SetActive(false);
    }

    void Update()
    {
        UpdateProgressBar();
        VerifyGameStatus();
    }

    private void UpdateProgressBar() {

        if (!isMiniGamePaused && !isShowingMiniGameEnding && miniGameStatus == 0) {

            // Calculate the percentage of the remaining time relative to the total time
            remainingTimeMilli = TOTAL_TIME_MILLI - swProgressBar.ElapsedMilliseconds;
            percentRemainingTime = (int)(remainingTimeMilli * 100 / TOTAL_TIME_MILLI);

            // Update the progressBar and the status of the game if the progress has changed
            if (!MathUtils.IsEqual(previousPercentRemainingTime, percentRemainingTime)) {
                previousPercentRemainingTime = percentRemainingTime;

                // Update the status of the game
                if (remainingTimeMilli <= 0) {
                    miniGameStatus = -1;
                }

                // Update the progressBar
                float deltaY = Math.Abs(progressBarTop.transform.position.y - progressBarBottom.transform.position.y);
                float onePercentDeltaY = percentRemainingTime * (deltaY / 100);

                progressBar.transform.position = new Vector3(progressBar.transform.position.x,
                                                             (float)(progressBarBottom.transform.position.y + onePercentDeltaY - deltaY),
                                                             progressBar.transform.position.z);
            }
        }
    }

    private void VerifyGameStatus() 
    {

        if (miniGameStatus == -1)
        {
            SoundController.StopSoundsMiniGames();
            SoundController.PlaySoundMiniGameLossRandom();
            MenuController.SOpenMiniGameNEnding(MINI_GAME_NUMBER, false);
        }
    }

    public void OnClickBtnPlayPause(bool play)
    {
        // Update the status of the UI elements of the progress controller
        btnPlay.SetActive(!play);
        btnPause.SetActive(play);
        btnExitToMainMenu.SetActive(!play);
        isMiniGamePaused = !play;

        // Update the status of the logic variables that control the progress controller
        if (play) {
            swProgressBar.Start();
        } else {
            swProgressBar.Stop();
        }

        // Update the status of the sounds related to the current mini-game
        SoundController.PauseSoundMiniGameN(!play, MINI_GAME_NUMBER);
    }

}