﻿using System.Diagnostics;
using UnityEngine;

public class MiniGamesController : MonoBehaviour 
{

    public static bool SIS_ATTACHED_TO_LEVEL_ENDING = false;
    public static int SGAME_LEVEL_NUMBER = 1;
    public static int SWAIT_TIME_IN_ITERATIONS_LOAD_SCENE = 3000;

    public int WAIT_TIME_IN_ITERATIONS_LOAD_SCENE = 3000;
    public int GAME_LEVEL_NUMBER = 1;
    public int MINI_GAME_NUMBER = 1;
    public bool IS_ATTACHED_TO_GAME_INTRO = false;
    public bool IS_ATTACHED_TO_GAME_ENDING = false;
    public bool IS_ATTACHED_TO_LEVEL_ENDING = false;

    private static bool sIsWaitLoadMiniGameNRunning = false;
    private static Stopwatch sStopWatchSwitchScene;

    private bool isWaitLoadMiniGameNRunning = false;
    private Stopwatch stopWatchSwitchScene;


    void Start() 
    {
        GameController.LoadGame();

        SIS_ATTACHED_TO_LEVEL_ENDING = IS_ATTACHED_TO_LEVEL_ENDING;
        SGAME_LEVEL_NUMBER = MINI_GAME_NUMBER;
        SWAIT_TIME_IN_ITERATIONS_LOAD_SCENE = WAIT_TIME_IN_ITERATIONS_LOAD_SCENE;
    }

    void Update() 
    {
        WaitIntroAndLoadMiniGameN();
        WaitEndingAndLoadGameLevel();
        WaitIdentifyEndLevel();
    }

    public void WaitIntroAndLoadMiniGameN() 
    {

        if (IS_ATTACHED_TO_GAME_INTRO) {

            if (!isWaitLoadMiniGameNRunning) {
                isWaitLoadMiniGameNRunning = true;

                stopWatchSwitchScene = new Stopwatch();
                stopWatchSwitchScene.Start();

            } else if (stopWatchSwitchScene.ElapsedMilliseconds > WAIT_TIME_IN_ITERATIONS_LOAD_SCENE) {
                MenuController.SOpenMiniGameN(MINI_GAME_NUMBER);
            }
        }
    }

    public void WaitEndingAndLoadGameLevel() 
    {

        if (IS_ATTACHED_TO_GAME_ENDING) {

            if (!isWaitLoadMiniGameNRunning) {
                isWaitLoadMiniGameNRunning = true;

                stopWatchSwitchScene = new Stopwatch();
                stopWatchSwitchScene.Start();
               
            } else if (stopWatchSwitchScene.ElapsedMilliseconds > WAIT_TIME_IN_ITERATIONS_LOAD_SCENE) {
                MenuController.SOpenLevel(GAME_LEVEL_NUMBER);
            }
        }
    }

    public static void WaitIdentifyEndLevel()
    {

        if (SIS_ATTACHED_TO_LEVEL_ENDING)
        {

            if (!sIsWaitLoadMiniGameNRunning)
            {
                sIsWaitLoadMiniGameNRunning = true;

                sStopWatchSwitchScene = new Stopwatch();
                sStopWatchSwitchScene.Start();

            } else if (sStopWatchSwitchScene.ElapsedMilliseconds > SWAIT_TIME_IN_ITERATIONS_LOAD_SCENE)
            {
                MenuController.SOpenLevels();
            }
        }
    }

    // Todo: As the mini games get developed, the methods below must be called 
    // from each minigame and the methods below must be deleted
    public void LoseMiniGame() 
    {
        GameController.UpdateStatusMiniGameN(MINI_GAME_NUMBER, false);
        MenuController.SOpenMiniGameNEnding(MINI_GAME_NUMBER, false);
    }

    public void WinMiniGame() 
    {
        GameController.UpdateStatusMiniGameN(MINI_GAME_NUMBER, true);
        MenuController.SOpenMiniGameNEnding(MINI_GAME_NUMBER, true);
    }
    //./Todo

}