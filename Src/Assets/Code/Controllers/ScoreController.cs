﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour 
{

    public Text liveValue;
    public Text winsValue;

    void LateUpdate()
    {
        liveValue.text = (GameController.player.GetNumBadges() + 60).ToString() + "  ANOS";
        winsValue.text = GameController.player.GetNumBadges().ToString() + "  PONTOS";
    }

}