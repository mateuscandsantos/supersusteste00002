﻿using UnityEngine;

public class ScrollingBGController : MonoBehaviour 
{

    public float INITIAL_BG_OFFSET = 32f;
    public float RATIO_DIST_BETWEEN_Z_LAYERS = 0.15f;

    public GameObject target;
    public GameObject bg0;
    public GameObject bg1;
    public GameObject bg2;
    public GameObject bg3;

    void Update () {
        bg0.transform.position = new Vector3(GetBg0Offset(), bg0.transform.position.y, bg0.transform.position.z);
        bg1.transform.position = new Vector3(GetBg1Offset(), bg1.transform.position.y, bg1.transform.position.z);
        bg2.transform.position = new Vector3(GetBg2Offset(), bg2.transform.position.y, bg2.transform.position.z);
        bg3.transform.position = new Vector3(GetBg3Offset(), bg3.transform.position.y, bg3.transform.position.z);
    }

    float GetBg0Offset()
    {
        return -1 * ((float)target.transform.position.x * RATIO_DIST_BETWEEN_Z_LAYERS - INITIAL_BG_OFFSET);
    }

    float GetBg1Offset() {
        return -1 * ((float)target.transform.position.x * RATIO_DIST_BETWEEN_Z_LAYERS - INITIAL_BG_OFFSET);
    }

    float GetBg2Offset() {
        return -1 * (((float)target.transform.position.x * RATIO_DIST_BETWEEN_Z_LAYERS * 3) - INITIAL_BG_OFFSET);
    }

    float GetBg3Offset() {
        return -1 * (((float)target.transform.position.x * RATIO_DIST_BETWEEN_Z_LAYERS * 3) - INITIAL_BG_OFFSET);
    }

}