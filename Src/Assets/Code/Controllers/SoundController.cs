﻿using System;
using UnityEngine;

public class SoundController : MonoBehaviour
{

    public static float DEFAULT_VOLUME = 0.5f;
    public static string SOUND_EXPLOSION = "event:/mg_vigi_s_fruit_explosion_liquid";

    public static FMOD.Studio.EventInstance soundTheme;
    public static FMOD.Studio.EventInstance soundBtnClick;
    public static FMOD.Studio.EventInstance soundBtnClickStartGame;
    public static FMOD.Studio.EventInstance soundMiniGameLoss1;
    public static FMOD.Studio.EventInstance soundMiniGameLoss2;
    public static FMOD.Studio.EventInstance soundMiniGameLoss3;
    public static FMOD.Studio.EventInstance soundMiniGameLoss4;
    public static FMOD.Studio.EventInstance soundMiniGameWin1;
    public static FMOD.Studio.EventInstance soundMiniGameWin2;
    public static FMOD.Studio.EventInstance soundMiniGameWin3;

    // Mini game 1
    public static FMOD.Studio.EventInstance mgTransplantSoundDuringSurgery;
    public static FMOD.Studio.EventInstance mgTransplantSoundGrabbingHearth;
    public static FMOD.Studio.EventInstance mgTransplantSoundFitHeartCase;
    public static FMOD.Studio.EventInstance mgTransplantSoundAfterSurgery;

    // Mini game 2
    public static FMOD.Studio.EventInstance mgVigiSanSoundExplosionSolid;
    public static FMOD.Studio.EventInstance mgVigiSanSoundExplosionSemiSolid;
    public static FMOD.Studio.EventInstance mgVigiSanSoundExplosionLiquid;
    public static FMOD.Studio.EventInstance mgVigiSanSoundExplosionBox;
    public static FMOD.Studio.EventInstance mgVigiSanSoundFleaLeaving;
    public static FMOD.Studio.EventInstance mgVigiSanSoundFleaAround;

    // Mini game 3
    public static FMOD.Studio.EventInstance mgMouthHPlacePiece6;
    public static FMOD.Studio.EventInstance mgMouthHPlacePiece5;
    public static FMOD.Studio.EventInstance mgMouthHPlacePiece4;
    public static FMOD.Studio.EventInstance mgMouthHPlacePiece3;
    public static FMOD.Studio.EventInstance mgMouthHPlacePiece2;
    public static FMOD.Studio.EventInstance mgMouthHPlacePiece1;
    public static FMOD.Studio.EventInstance mgMouthHOpenedMouth;
    public static FMOD.Studio.EventInstance mgMouthHJobDone;

    // Mini game 4
    public static FMOD.Studio.EventInstance mgTransfusionStartGame;

    // Mini game 5
    public static FMOD.Studio.EventInstance mgVaccinationStartGame;
    public static FMOD.Studio.EventInstance mgVaccinationHitCorrectly;
    public static FMOD.Studio.EventInstance mgVaccinationWrongHit1;
    public static FMOD.Studio.EventInstance mgVaccinationWrongHit2;
    public static FMOD.Studio.EventInstance mgVaccinationWrongHit3;

    // Mini game 7
    public static FMOD.Studio.EventInstance mgSamuAtHomeRescue1;
    public static FMOD.Studio.EventInstance mgSamuAtHomeRescue2;
    public static FMOD.Studio.EventInstance mgSamuAtHomeRescue3;
    public static FMOD.Studio.EventInstance mgSamuAtHomeDriving;
    public static FMOD.Studio.EventInstance mgSamuAtHomeDrifting;

    // Mini game 8
    // ..

    static bool hasInitialized = false;

    public static void InitSound()
    {

        if (!hasInitialized)
        {
            hasInitialized = true;

            soundTheme = FMODUnity.RuntimeManager.CreateInstance("event:/theme");
            soundBtnClick = FMODUnity.RuntimeManager.CreateInstance("event:/btn_click");
            soundBtnClickStartGame = FMODUnity.RuntimeManager.CreateInstance("event:/btn_click_start_game");
            soundMiniGameLoss1 = FMODUnity.RuntimeManager.CreateInstance("event:/mini_game_loss_1");
            soundMiniGameLoss2 = FMODUnity.RuntimeManager.CreateInstance("event:/mini_game_loss_2");
            soundMiniGameLoss3 = FMODUnity.RuntimeManager.CreateInstance("event:/mini_game_loss_3");
            soundMiniGameLoss4 = FMODUnity.RuntimeManager.CreateInstance("event:/mini_game_loss_4");
            soundMiniGameWin1 = FMODUnity.RuntimeManager.CreateInstance("event:/mini_game_win_1");
            soundMiniGameWin2 = FMODUnity.RuntimeManager.CreateInstance("event:/mini_game_win_2");
            soundMiniGameWin3 = FMODUnity.RuntimeManager.CreateInstance("event:/mini_game_win_3");

            UpdateThemeVolume();
            UpdateSoundEffectsVolume();
        }

        MuteAllInstances(!GameController.settings.IsSoundEnabled());
    }

    public static void UpdateThemeVolume()
    {
        soundTheme.setVolume(GameController.settings.GetSoundLevel());
    }

    public static void UpdateSoundEffectsVolume()
    {
        soundBtnClick.setVolume(GameController.settings.GetSoundEffectsLevel());
        soundBtnClickStartGame.setVolume(GameController.settings.GetSoundEffectsLevel());
        soundMiniGameLoss1.setVolume(GameController.settings.GetSoundEffectsLevel());
        soundMiniGameLoss2.setVolume(GameController.settings.GetSoundEffectsLevel());
        soundMiniGameLoss3.setVolume(GameController.settings.GetSoundEffectsLevel());
        soundMiniGameLoss4.setVolume(GameController.settings.GetSoundEffectsLevel());
        soundMiniGameWin1.setVolume(GameController.settings.GetSoundEffectsLevel());
        soundMiniGameWin2.setVolume(GameController.settings.GetSoundEffectsLevel());
        soundMiniGameWin3.setVolume(GameController.settings.GetSoundEffectsLevel());
    }

    public static void MuteAllInstances(bool mute)
    {

        if (mute)
        {
            soundTheme.setVolume(0f);
            soundBtnClick.setVolume(0f);
            soundBtnClickStartGame.setVolume(0f);
            soundMiniGameLoss1.setVolume(0f);
            soundMiniGameLoss2.setVolume(0f);
            soundMiniGameLoss3.setVolume(0f);
            soundMiniGameLoss4.setVolume(0f);
            soundMiniGameWin1.setVolume(0f);
            soundMiniGameWin2.setVolume(0f);
            soundMiniGameWin3.setVolume(0f);
        }
        else if (MathUtils.IsEqual(GameController.settings.GetSoundLevel(), 0))
        {
            GameController.settings.SetSoundLevel(DEFAULT_VOLUME);
        }

        else
        {
            UpdateThemeVolume();
            UpdateSoundEffectsVolume();
        }
    }

    public static void PlayTheme()
    {
        if (GameController.settings.IsSoundEnabled())
        {
            soundTheme.start();
        }
    }

    public void PlaySoundBtnClick()
    {
        if (GameController.settings.IsSoundEnabled())
        {
            soundBtnClick.start();
        }
    }

    public static void SPlaySoundBtnClick()
    {
        if (GameController.settings.IsSoundEnabled())
        {
            soundBtnClick.start();
        }
    }

    public void PlaySoundBtnClickStartGame()
    {
        if (GameController.settings.IsSoundEnabled())
        {
            soundBtnClickStartGame.start();
        }
    }

    public static void PlaySoundMiniGameLossRandom()
    {
        int typeNumber = (int) UnityEngine.Random.Range(1f, 4f);
        PlaySoundMiniGameLoss(typeNumber);
    }

    public static void PlaySoundMiniGameLoss(int type)
    {

        switch (type)
        {

            case 1:
                {
                    soundMiniGameLoss1.start();
                    break;
                }

            case 2:
                {
                    soundMiniGameLoss2.start();
                    break;
                }

            case 3:
                {
                    soundMiniGameLoss3.start();
                    break;
                }

            case 4:
                {
                    soundMiniGameLoss4.start();
                    break;
                }
        }
    }

    public static void PlaySoundMiniGameWinRandom()
    {
        int typeNumber = (int) UnityEngine.Random.Range(1f, 3f);
        PlaySoundMiniGameWin(typeNumber);
    }

    public static void PlaySoundMiniGameWin(int type)
    {

        switch (type)
        {

            case 1:
                {
                    soundMiniGameWin1.start();
                    break;
                }

            case 2:
                {
                    soundMiniGameWin2.start();
                    break;
                }

            case 3:
                {
                    soundMiniGameWin3.start();
                    break;
                }
        }
    }

    public static void PlaySoundPlaceToothBracketRandom()
    {
        int typeNumber = (int)UnityEngine.Random.Range(1f, 6f);
        PlaySoundPlaceToothBracket(typeNumber);
    }

    public static void PlaySoundPlaceToothBracket(int typeNumber) 
    {

        switch (typeNumber) 
        {

            case 1: 
                {
                    mgMouthHPlacePiece1.start();
                    break;
                }

            case 2:
                {
                    mgMouthHPlacePiece2.start();
                    break;
                }

            case 3:
                {
                    mgMouthHPlacePiece3.start();
                    break;
                }

            case 4:
                {
                    mgMouthHPlacePiece4.start();
                    break;
                }

            case 5:
                {
                    mgMouthHPlacePiece5.start();
                    break;
                }

            case 6:
                {
                    mgMouthHPlacePiece6.start();
                    break;
                }
        }
    }

    public static void PlaySoundRescueVictimRandom()
    {
        int typeNumber = (int) UnityEngine.Random.Range(1f, 3f);
        PlaySoundRescueVictim(typeNumber);
    }

    public static void PlaySoundRescueVictim(int type)
    {

        switch (type)
        {

            case 1:
                {
                    mgSamuAtHomeRescue1.start();
                    break;
                }

            case 2:
                {
                    mgSamuAtHomeRescue2.start();
                    break;
                }

            case 3:
                {
                    mgSamuAtHomeRescue3.start();
                    break;
                }
        }
    }

    #region MiniGames
    public static void StopSoundsMiniGames() {
        StopSoundsMiniGame1();
        StopSoundsMiniGame2();
        StopSoundsMiniGame3();
        StopSoundsMiniGame4();
        StopSoundsMiniGame5();
        StopSoundsMiniGame7();
        StopSoundsMiniGame8();
    }

    public static void StopSoundsMiniGame1()
    {
        try 
        {
            mgTransplantSoundGrabbingHearth.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgTransplantSoundFitHeartCase.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgTransplantSoundAfterSurgery.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgTransplantSoundDuringSurgery.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        } 
        catch(Exception e) {
            Debug.Log(e.ToString());
        }
    }

    public static void StopSoundsMiniGame2() 
    {
        try 
        {
            mgVigiSanSoundExplosionSolid.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgVigiSanSoundExplosionSemiSolid.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgVigiSanSoundExplosionLiquid.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgVigiSanSoundExplosionBox.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgVigiSanSoundFleaLeaving.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgVigiSanSoundFleaAround.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public static void StopSoundsMiniGame3()
    {
        try
        {
            mgMouthHPlacePiece6.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgMouthHPlacePiece5.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgMouthHPlacePiece4.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgMouthHPlacePiece3.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgMouthHPlacePiece2.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgMouthHPlacePiece1.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgMouthHOpenedMouth.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgMouthHJobDone.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public static void StopSoundsMiniGame4()
    {
        try 
        {
            mgTransfusionStartGame.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public static void StopSoundsMiniGame5() 
    {
        try 
        {
            mgVaccinationStartGame.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgVaccinationHitCorrectly.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgVaccinationWrongHit1.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgVaccinationWrongHit2.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgVaccinationWrongHit3.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public static void StopSoundsMiniGame6() 
    {
        try {
            // ..
        } catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public static void StopSoundsMiniGame7()
    {
        try {
            mgSamuAtHomeRescue1.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgSamuAtHomeRescue2.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgSamuAtHomeRescue3.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgSamuAtHomeDriving.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgSamuAtHomeDrifting.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        } 
        catch (Exception e) 
        {
            Debug.Log(e.ToString());
        }
    }

    public static void StopSoundsMiniGame8()
    {
        try
        {
            Debug.Log("StopSoundsMiniGame8");
            /*
            mgSamuAtHomeRescue1.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgSamuAtHomeRescue2.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgSamuAtHomeRescue3.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgSamuAtHomeDriving.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            mgSamuAtHomeDrifting.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            */
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public static void PauseSoundMiniGameN(bool pause, int miniGameNumber)
    {

        switch (miniGameNumber) {

            case 1: 
                {
                    PauseSoundMiniGame1(pause);
                    break;
                }

            case 2:
                {
                    PauseSoundMiniGame2(pause);
                    break;
                }

            case 3:
                {
                    PauseSoundMiniGame3(pause);
                    break;
                }

            case 4:
                {
                    PauseSoundMiniGame4(pause);
                    break;
                }

            case 5:
                {
                    PauseSoundMiniGame5(pause);
                    break;
                }

            case 6:
                {
                    PauseSoundMiniGame6();
                    break;
                }

            case 7:
                {
                    PauseSoundMiniGame7(pause);
                    break;
                }

            case 8:
                {
                    PauseSoundMiniGame8(pause);
                    break;
                }
        }
    }

    public static void PauseSoundMiniGame1(bool pause)
    {
        try
        {
            mgTransplantSoundGrabbingHearth.setPaused(pause);
            mgTransplantSoundFitHeartCase.setPaused(pause);
            mgTransplantSoundAfterSurgery.setPaused(pause);
            mgTransplantSoundDuringSurgery.setPaused(pause);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public static void PauseSoundMiniGame2(bool pause)
    {
        try
        {
            mgVigiSanSoundExplosionSolid.setPaused(pause);
            mgVigiSanSoundExplosionSemiSolid.setPaused(pause);
            mgVigiSanSoundExplosionLiquid.setPaused(pause);
            mgVigiSanSoundExplosionBox.setPaused(pause);
            mgVigiSanSoundFleaLeaving.setPaused(pause);
            mgVigiSanSoundFleaAround.setPaused(pause);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public static void PauseSoundMiniGame3(bool pause)
    {
        try
        {
            mgMouthHPlacePiece6.setPaused(pause);
            mgMouthHPlacePiece5.setPaused(pause);
            mgMouthHPlacePiece4.setPaused(pause);
            mgMouthHPlacePiece3.setPaused(pause);
            mgMouthHPlacePiece2.setPaused(pause);
            mgMouthHPlacePiece1.setPaused(pause);
            mgMouthHOpenedMouth.setPaused(pause);
            mgMouthHJobDone.setPaused(pause);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public static void PauseSoundMiniGame4(bool pause)
    {
        try
        {
            mgTransfusionStartGame.setPaused(pause);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public static void PauseSoundMiniGame5(bool pause)
    {
        try
        {
            mgVaccinationStartGame.setPaused(pause);
            mgVaccinationHitCorrectly.setPaused(pause);
            mgVaccinationWrongHit1.setPaused(pause);
            mgVaccinationWrongHit2.setPaused(pause);
            mgVaccinationWrongHit3.setPaused(pause);
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public static void PauseSoundMiniGame6()
    {
        try
        {
            // ..
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public static void PauseSoundMiniGame7(bool pause)
    {
        try
        {
            mgSamuAtHomeRescue1.setPaused(pause);
            mgSamuAtHomeRescue2.setPaused(pause);
            mgSamuAtHomeRescue3.setPaused(pause);
            mgSamuAtHomeDriving.setPaused(pause);
            mgSamuAtHomeDrifting.setPaused(pause);
        } 
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }

    public static void PauseSoundMiniGame8(bool pause)
    {
        try
        {
            Debug.Log("PauseSoundMiniGame8");
            /*
            mgSamuAtHomeRescue1.setPaused(pause);
            mgSamuAtHomeRescue2.setPaused(pause);
            mgSamuAtHomeRescue3.setPaused(pause);
            mgSamuAtHomeDriving.setPaused(pause);
            mgSamuAtHomeDrifting.setPaused(pause);
            */
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
    }
    #endregion
}