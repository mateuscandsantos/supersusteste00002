﻿using UnityEngine;

public class Game1Player : MonoBehaviour {

    public float acceleration_ratio = 0.5f;
    public float max_horizontal_delta = 4f;
    public bool facing_right = true;

    private float mPlayerVerticalVelocity = 14f;
    private float mPlayerGravity = 14f;
    private float mPlayerJumpForce = 1f;
    private float mPlayerDistToGround = 0f;

    private Rigidbody2D mPlayerRb;
	private SpriteRenderer mPlayerSpriteRenderer;
    private float mCurrentHorizontalSpeed = 0f;

    /**
     * Initialization
     */
	void Start () {
        mPlayerRb = GetComponent<Rigidbody2D> ();
		mPlayerSpriteRenderer = GetComponent<SpriteRenderer> ();
	}
	
    /**
     * Continuosly executing
     */
    void Update () {
        mPlayerRb.velocity = GetHorizontalSpeedVector();
        mPlayerSpriteRenderer.flipX = FlipPlayerDirection();
        mPlayerDistToGround = mPlayerRb.transform.position.y;

        DetectJump();
	}

    Vector2 GetHorizontalSpeedVector() {
        // Calculate the updated speed
        float delta = (JoystickController.horizontalDir * Time.time) / 1000f;

        if (delta > 0) {
            delta += acceleration_ratio;
            mCurrentHorizontalSpeed += delta;

        } else if (delta < 0) {
            delta += -acceleration_ratio;
            mCurrentHorizontalSpeed += delta;

        } else {
            mCurrentHorizontalSpeed = 0;
        }

        // Limit the horizontal delta
        if (mCurrentHorizontalSpeed > max_horizontal_delta) {
            mCurrentHorizontalSpeed = max_horizontal_delta;

        } else if (mCurrentHorizontalSpeed < -max_horizontal_delta) {
            mCurrentHorizontalSpeed = -max_horizontal_delta;
        }

        return new Vector2(mCurrentHorizontalSpeed, mPlayerRb.velocity.y);
    }

    bool FlipPlayerDirection() {

        if (facing_right && JoystickController.horizontalDir < 0 ||
            !facing_right && JoystickController.horizontalDir > 0) {
            return true;
        } else {
            return false;   
        }
    }

    void DetectJump() {

        if (IsPlayerGrounded() && JoystickController.verticalDir > 0) {
            mPlayerVerticalVelocity = mPlayerJumpForce;
        } else {
            mPlayerVerticalVelocity = -mPlayerGravity * Time.deltaTime;
        }

        mPlayerRb.AddForce(new Vector2(0, mPlayerVerticalVelocity), ForceMode2D.Impulse);   
    }

    bool IsPlayerGrounded() {
        return mPlayerDistToGround < -1;
    }

    /**
     * Events
     */
    void OnCollisionEnter2D(Collision2D collision) {

		if (collision.gameObject.tag == CollidablesUtils.TAG_WALL) {
            CleanScene();
            MenuController.SOpenGameOver();

        } else if (collision.gameObject.tag == CollidablesUtils.TAG_GAME_1) {
            CleanScene();
        }
	}

	void OnTriggerEnter2D(Collider2D collid) {

        if (collid.gameObject.tag == CollidablesUtils.TAG_SPIKE) {
            CleanScene();
            MenuController.SOpenGameOver();

        } else if (collid.gameObject.tag == CollidablesUtils.TAG_GAME_1) {
            CleanScene();
        }
	}

    void CleanScene() {
        var spikeClones = GameObject.FindGameObjectsWithTag(CollidablesUtils.TAG_SPIKE);

        foreach (GameObject clone in spikeClones) {
            Destroy(clone);
        }
    }

}