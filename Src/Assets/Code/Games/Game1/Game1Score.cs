﻿using UnityEngine;

public class Game1Score : MonoBehaviour {

	public static int score;
	public UnityEngine.UI.Text scoreTxt;

	void Start () {
		Game1Score.score = 0;
	}

	void Update () {
		scoreTxt.text = Game1Score.score.ToString();
	}

}