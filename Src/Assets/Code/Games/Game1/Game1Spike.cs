﻿using UnityEngine;

public class Game1Spike : MonoBehaviour {

	// Public parameters
	public int maxDrag;
	public int drag;

	public Vector3 position;
	public GameObject spikePrefab;
	private Rigidbody2D mSpikeRb;

	void Start () {
		mSpikeRb = GetComponent<Rigidbody2D> ();

		// Set a random drag to a Spike
		drag = Random.Range (2, maxDrag + 1);
		mSpikeRb.drag = drag;

		position = transform.position;
	}

	void OnBecameInvisible() {
		Debug.Log ("Instantiate: ");
		Debug.Log (spikePrefab);
		Debug.Log (transform.localRotation);

		Instantiate (spikePrefab, position, transform.localRotation);

		Game1Score.score += 1;

		Debug.Log ("Increased gamescore");

		Destroy (this.gameObject);
	}

}