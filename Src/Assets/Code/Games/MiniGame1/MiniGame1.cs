﻿using UnityEngine;

public class MiniGame1 : ExecuteAfterNSecsBehavior
{

    public float SCALE_SPEED = 0.2f;

    public GameObject newHeartCase;
    public GameObject patientHeartCase;
    public GameObject replacedHeartCase;
    public GameObject goodHeartAfter;

    public DragAndDropSpriteBehavior newHeart;
    public DragAndDropSpriteBehavior replacedHeart;

    bool mIsNewHeartInsideNewHeartCase;
    bool mIsNewHeartInsidePatientHeartCase;
    bool mIsReplacedHeartInsideReplacedHeartCase;

    GameObjectsUtils gameObjectUtils;

    bool playingEndingSound = false;
    bool goodHeartIsSmall = true;
    bool patientHeartIsSmall = true;
    bool isBeatingHeart = false;

    #region Initialization
    void Start()
    {
        GameController.LoadGame();
        InitGameVars();
        InitSound();
        InitGameStatus();
    }

    void InitGameVars() {
        MiniGameProgressController.miniGameStatus = 0;
        gameObjectUtils = new GameObjectsUtils();
    }

    void InitSound() {
        SoundController.mgTransplantSoundDuringSurgery = FMODUnity.RuntimeManager.CreateInstance("event:/mg_heart_t_during_transplant");
        SoundController.mgTransplantSoundGrabbingHearth = FMODUnity.RuntimeManager.CreateInstance("event:/mg_heart_t_grabbing_heart");
        SoundController.mgTransplantSoundFitHeartCase = FMODUnity.RuntimeManager.CreateInstance("event:/mg_heart_t_placing_heart");
        SoundController.mgTransplantSoundAfterSurgery = FMODUnity.RuntimeManager.CreateInstance("event:/mg_heart_t_after_transplant");

        SoundController.mgTransplantSoundDuringSurgery.start();
    }

    void InitGameStatus()
    {
        mIsNewHeartInsideNewHeartCase = false;
        mIsNewHeartInsidePatientHeartCase = false;
        mIsReplacedHeartInsideReplacedHeartCase = false;
    }
    #endregion

    #region Update
    void Update()
    {

        if (!MiniGameProgressController.isMiniGamePaused)
        {
            HeartBeat();
            UpdateStateGameObjects();
            ValidateGameLogic();
        }
    }

    void HeartBeat() 
    {

        if (!isBeatingHeart && MiniGameProgressController.miniGameStatus < 1) 
        {
            isBeatingHeart = true;

            Wait(0.8f, () =>
            {

                if (goodHeartIsSmall)
                {
                    goodHeartIsSmall = false;
                    newHeart.transform.localScale += 2 * (new Vector3(SCALE_SPEED, SCALE_SPEED, 1f));
                }
                else
                {
                    goodHeartIsSmall = true;
                    newHeart.transform.localScale += 2 * (new Vector3(-SCALE_SPEED, -SCALE_SPEED, 1f));
                }

                if (patientHeartIsSmall)
                {
                    patientHeartIsSmall = false;
                    replacedHeart.transform.localScale += 2 * (new Vector3(SCALE_SPEED, SCALE_SPEED, 1f));
                }
                else
                {
                    patientHeartIsSmall = true;
                    replacedHeart.transform.localScale += 2 * (new Vector3(-SCALE_SPEED, -SCALE_SPEED, 1f));
                }

                isBeatingHeart = false;
            });
        }
    }

    void UpdateStateGameObjects()
    {

        if (!replacedHeart.isDragging)
        {

            if (gameObjectUtils.IsGameObjectAInsideB(replacedHeart.transform.position, replacedHeartCase))
            {
                mIsReplacedHeartInsideReplacedHeartCase = true;
                replacedHeart.transform.position = replacedHeartCase.transform.position;
            }
        }

        if (newHeart.isDragging)
        {

            if (gameObjectUtils.IsGameObjectAInsideB(newHeart.transform.position, newHeartCase) && 
                !mIsNewHeartInsideNewHeartCase)
            {
                mIsNewHeartInsideNewHeartCase = true;
                SoundController.mgTransplantSoundGrabbingHearth.start();
            }
        }

        if (!newHeart.isDragging)
        {

            if (gameObjectUtils.IsGameObjectAInsideB(newHeart.transform.position, patientHeartCase))
            {
                mIsNewHeartInsidePatientHeartCase = true;
                newHeart.transform.position = patientHeartCase.transform.position;
                SoundController.mgTransplantSoundFitHeartCase.start();
            }
        }
    }

    void ValidateGameLogic()
    {

        if (mIsNewHeartInsidePatientHeartCase && mIsReplacedHeartInsideReplacedHeartCase)
        {

            if (!playingEndingSound)
            {
                playingEndingSound = true;

                MiniGameProgressController.miniGameStatus = 1;

                ShowMiniGameEnding();

                Wait(3f, () =>
                {
                    SoundController.StopSoundsMiniGame1();
                    SoundController.PlaySoundMiniGameWinRandom();

                    GameController.UpdateStatusMiniGameN(1, true);
                    MenuController.SOpenMiniGameNEnding(1, true);
                });
            }
        }
    }

    void ShowMiniGameEnding()
    {
        Wait(1f, () =>
        {
            goodHeartAfter.SetActive(true);

            Wait(0.03f, () =>
            {
                goodHeartAfter.SetActive(false);

                Wait(0.06f, () =>
                {
                    goodHeartAfter.SetActive(true);
                });
            });
        });

        SoundController.mgTransplantSoundDuringSurgery.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        SoundController.mgTransplantSoundAfterSurgery.start();
    }
    #endregion

}