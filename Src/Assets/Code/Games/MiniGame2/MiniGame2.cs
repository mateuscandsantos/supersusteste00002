﻿using UnityEngine;

public class MiniGame2 : ExecuteAfterNSecsBehavior
{

    public Camera Camera;

    public GameObject goodOranges;
    public GameObject badMilk;
    public GameObject goodOliveCan1;
    public GameObject goodOliveCan2;
    public GameObject badOranges;
    public GameObject badPears;
    public GameObject goodMilk;
    public GameObject goodPears;
    public GameObject badApples;

    public GameObject explosionBadMilk;
    public GameObject explosionBadOranges;
    public GameObject explosionBadPears;
    public GameObject explosionBadApples;

    GameObjectsUtils mGameObjUtils;

    bool playingEndingSound = false;

    #region Initialization
    void Start()
    {
        GameController.LoadGame();
        InitGameVars();
        InitGameSound();
        InitGameStatus();
    }

    void InitGameVars()
    {
        MiniGameProgressController.miniGameStatus = 0;
        mGameObjUtils = new GameObjectsUtils();
    }

    void InitGameSound() {
        SoundController.mgVigiSanSoundExplosionSolid = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vigi_s_fruit_explosion_solid");
        SoundController.mgVigiSanSoundExplosionSemiSolid = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vigi_s_fruit_explosion_semi_solid");
        SoundController.mgVigiSanSoundExplosionLiquid = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vigi_s_fruit_explosion_liquid");
        SoundController.mgVigiSanSoundExplosionBox = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vigi_s_box_explosion");
        SoundController.mgVigiSanSoundFleaLeaving = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vigi_s_flea_leaving");
        SoundController.mgVigiSanSoundFleaAround = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vigi_s_flea_flying_around");

        SoundController.mgVigiSanSoundFleaAround.start();
    }

    void InitGameStatus()
    {
        badMilk.SetActive(true);
        badOranges.SetActive(true);
        badPears.SetActive(true);
        badApples.SetActive(true);
    }
    #endregion

    #region Update
    void Update()
    {

        if (!MiniGameProgressController.isMiniGamePaused)
        {
            UpdateStateGameObjects();
            ValidateGameLogic();
        }
    }

    private void UpdateStateGameObjects()
    {

        if (mGameObjUtils.ClickedGameObject(Camera, goodOranges)) {
            Debug.Log("Todo: play sound that indicates that the user clicked on the wrong place ...");
        }

        if (mGameObjUtils.ClickedGameObject(Camera, badMilk))
        {
            explodeObject(badMilk, explosionBadMilk);
            SoundController.mgVigiSanSoundExplosionBox.start();
        }

        if (mGameObjUtils.ClickedGameObject(Camera, goodOliveCan1))
        {
            Debug.Log("Todo: play sound that indicates that the user clicked on the wrong place ...");
        }

        if (mGameObjUtils.ClickedGameObject(Camera, goodOliveCan2))
        {
            Debug.Log("Todo: play sound that indicates that the user clicked on the wrong place ...");
        }

        if (mGameObjUtils.ClickedGameObject(Camera, badOranges))
        {
            explodeObject(badOranges, explosionBadOranges);
            SoundController.mgVigiSanSoundExplosionLiquid.start();
            SoundController.mgVigiSanSoundFleaAround.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            SoundController.mgVigiSanSoundFleaLeaving.start();
        }

        if (mGameObjUtils.ClickedGameObject(Camera, badPears))
        {
            explodeObject(badPears, explosionBadPears);
            SoundController.mgVigiSanSoundExplosionSemiSolid.start();
        }

        if (mGameObjUtils.ClickedGameObject(Camera, goodMilk))
        {
            Debug.Log("Todo: play sound that indicates that the user clicked on the wrong place ...");
        }

        if (mGameObjUtils.ClickedGameObject(Camera, goodPears))
        {
            Debug.Log("Todo: play sound that indicates that the user clicked on the wrong place ...");
        }

        if (mGameObjUtils.ClickedGameObject(Camera, badApples))
        {
            explodeObject(badApples, explosionBadApples);
            SoundController.mgVigiSanSoundExplosionSemiSolid.start();
        }
    }

    void explodeObject(GameObject obj, GameObject objExplosion) {
        objExplosion.SetActive(true);

        Wait(0.1f, () => {
            obj.SetActive(false);
        });
        Wait(0.2f, () => {
            objExplosion.SetActive(false);
        });
    }

    void ValidateGameLogic()
    {

        if (!badMilk.activeSelf && !badOranges.activeSelf && !badPears.activeSelf && !badApples.activeSelf) {

            if (!playingEndingSound)
            {
                playingEndingSound = true;

                MiniGameProgressController.miniGameStatus = 1;

                ShowMiniGameEnding();

                Wait(2f, () =>
                {
                    SoundController.StopSoundsMiniGame2();
                    SoundController.PlaySoundMiniGameWinRandom();

                    GameController.UpdateStatusMiniGameN(2, true);
                    MenuController.SOpenMiniGameNEnding(2, true);
                });
            }
        }
    }

    void ShowMiniGameEnding()
    {
        FlyingBehaviour.killMosquitos = true;
    }

    #endregion

}