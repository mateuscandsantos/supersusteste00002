﻿using UnityEngine;

public class MiniGame3 : ExecuteAfterNSecsBehavior {

    public Camera Camera;

    public GameObject leftUpperTooth1;
    public GameObject leftUpperTooth2;
    public GameObject leftUpperTooth3;
    public GameObject rightUpperTooth1;
    public GameObject rightUpperTooth2;
    public GameObject rightUpperTooth3;

    public GameObject leftUpperTooth1Completed;
    public GameObject leftUpperTooth2Completed;
    public GameObject leftUpperTooth3Completed;
    public GameObject rightUpperTooth1Completed;
    public GameObject rightUpperTooth2Completed;
    public GameObject rightUpperTooth3Completed;

    public GameObject lineBrackets12;
    public GameObject lineBrackets23;
    public GameObject lineBrackets34;
    public GameObject lineBrackets45;
    public GameObject lineBrackets56;

    GameObjectsUtils mGameObjUtils;

    bool playingEndingSound = false;

    #region Initialization
    void Start()
    {
        GameController.LoadGame();
        InitGameVars();
        InitGameSound();
        InitGameStatus();
    }

    void InitGameVars() {
        MiniGameProgressController.miniGameStatus = 0;
        mGameObjUtils = new GameObjectsUtils();
    }

    void InitGameSound()
    {
        SoundController.mgMouthHPlacePiece6 = FMODUnity.RuntimeManager.CreateInstance("event:/mg_mouth_h_place_piece_6");
        SoundController.mgMouthHPlacePiece5 = FMODUnity.RuntimeManager.CreateInstance("event:/mg_mouth_h_place_piece_5");
        SoundController.mgMouthHPlacePiece4 = FMODUnity.RuntimeManager.CreateInstance("event:/mg_mouth_h_place_piece_4");
        SoundController.mgMouthHPlacePiece3 = FMODUnity.RuntimeManager.CreateInstance("event:/mg_mouth_h_place_piece_3");
        SoundController.mgMouthHPlacePiece2 = FMODUnity.RuntimeManager.CreateInstance("event:/mg_mouth_h_place_piece_2");
        SoundController.mgMouthHPlacePiece1 = FMODUnity.RuntimeManager.CreateInstance("event:/mg_mouth_h_place_piece_1");
        SoundController.mgMouthHOpenedMouth = FMODUnity.RuntimeManager.CreateInstance("event:/mg_mouth_h_opened_mouth");
        SoundController.mgMouthHJobDone = FMODUnity.RuntimeManager.CreateInstance("event:/mg_mouth_h_job_done");

        SoundController.mgMouthHOpenedMouth.start();
    }

    void InitGameStatus()
    {
        leftUpperTooth1.SetActive(true);
        leftUpperTooth2.SetActive(true);
        leftUpperTooth3.SetActive(true);
        rightUpperTooth1.SetActive(true);
        rightUpperTooth2.SetActive(true);
        rightUpperTooth3.SetActive(true);

        leftUpperTooth1Completed.SetActive(false);
        leftUpperTooth2Completed.SetActive(false);
        leftUpperTooth3Completed.SetActive(false);
        rightUpperTooth1Completed.SetActive(false);
        rightUpperTooth2Completed.SetActive(false);
        rightUpperTooth3Completed.SetActive(false);

        lineBrackets12.SetActive(false);
        lineBrackets23.SetActive(false);
        lineBrackets34.SetActive(false);
        lineBrackets45.SetActive(false);
        lineBrackets56.SetActive(false);
    }
    #endregion

    #region Update
    void Update()
    {

        if (!MiniGameProgressController.isMiniGamePaused)
        {
            UpdateStateGameObjects();
            ValidateGameLogic();
        }
    }

    private void UpdateStateGameObjects()
    {

        if (mGameObjUtils.ClickedGameObject(Camera, leftUpperTooth1)) {
            leftUpperTooth1.SetActive(false);
            leftUpperTooth1Completed.SetActive(true);

            if (leftUpperTooth1Completed.activeSelf && leftUpperTooth2Completed.activeSelf) {
                lineBrackets12.SetActive(true);
                SoundController.PlaySoundPlaceToothBracket(1);
            }
        }

        if (mGameObjUtils.ClickedGameObject(Camera, leftUpperTooth2)) {
            leftUpperTooth2.SetActive(false);
            leftUpperTooth2Completed.SetActive(true);

            if (leftUpperTooth1Completed.activeSelf && leftUpperTooth2Completed.activeSelf)
            {
                lineBrackets12.SetActive(true);
                SoundController.PlaySoundPlaceToothBracket(1);
            }

            if (leftUpperTooth2Completed.activeSelf && leftUpperTooth3Completed.activeSelf)
            {
                lineBrackets23.SetActive(true);
                SoundController.PlaySoundPlaceToothBracket(2);
            }
        }

        if (mGameObjUtils.ClickedGameObject(Camera, leftUpperTooth3)) {
            leftUpperTooth3.SetActive(false);
            leftUpperTooth3Completed.SetActive(true);

            if (leftUpperTooth2Completed.activeSelf && leftUpperTooth3Completed.activeSelf)
            {
                lineBrackets23.SetActive(true);
                SoundController.PlaySoundPlaceToothBracket(2);
            }

            if (leftUpperTooth3Completed.activeSelf && rightUpperTooth3Completed.activeSelf)
            {
                lineBrackets34.SetActive(true);
                SoundController.PlaySoundPlaceToothBracket(3);
            }
        }

        if (mGameObjUtils.ClickedGameObject(Camera, rightUpperTooth3)) {
            rightUpperTooth3.SetActive(false);
            rightUpperTooth3Completed.SetActive(true);

            if (leftUpperTooth3Completed.activeSelf && rightUpperTooth3Completed.activeSelf)
            {
                lineBrackets34.SetActive(true);
                SoundController.PlaySoundPlaceToothBracket(3);
            }

            if (rightUpperTooth3Completed.activeSelf && rightUpperTooth2Completed.activeSelf)
            {
                lineBrackets45.SetActive(true);
                SoundController.PlaySoundPlaceToothBracket(4);
            }
        }

        if (mGameObjUtils.ClickedGameObject(Camera, rightUpperTooth2)) {
            rightUpperTooth2.SetActive(false);
            rightUpperTooth2Completed.SetActive(true);

            if (rightUpperTooth3Completed.activeSelf && rightUpperTooth2Completed.activeSelf)
            {
                lineBrackets45.SetActive(true);
                SoundController.PlaySoundPlaceToothBracket(4);
            }

            if (rightUpperTooth2Completed.activeSelf && rightUpperTooth1Completed.activeSelf)
            {
                lineBrackets56.SetActive(true);
                SoundController.PlaySoundPlaceToothBracket(5);
            }
        }

        if (mGameObjUtils.ClickedGameObject(Camera, rightUpperTooth1))
        {
            rightUpperTooth1.SetActive(false);
            rightUpperTooth1Completed.SetActive(true);

            if (rightUpperTooth2Completed.activeSelf && rightUpperTooth1Completed.activeSelf)
            {
                lineBrackets56.SetActive(true);
                SoundController.PlaySoundPlaceToothBracket(6);
            }
        }
    }

    void ValidateGameLogic()
    {

        if (leftUpperTooth1Completed.activeSelf &&
            leftUpperTooth2Completed.activeSelf &&
            leftUpperTooth3Completed.activeSelf &&
            rightUpperTooth1Completed.activeSelf &&
            rightUpperTooth2Completed.activeSelf &&
            rightUpperTooth3Completed.activeSelf) {


            if (!playingEndingSound) {
                playingEndingSound = true;

                MiniGameProgressController.miniGameStatus = 1;

                ShowMiniGameEnding();

                Wait(2f, () =>
                {
                    SoundController.StopSoundsMiniGame3();
                    SoundController.PlaySoundMiniGameWinRandom();

                    GameController.UpdateStatusMiniGameN(3, true);
                    MenuController.SOpenMiniGameNEnding(3, true);
                });
            }
        }
    }

    void ShowMiniGameEnding()
    {
        SoundController.mgMouthHOpenedMouth.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        SoundController.mgMouthHJobDone.start();
    }

    #endregion

}