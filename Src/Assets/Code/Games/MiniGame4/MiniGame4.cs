﻿using UnityEngine;

public class MiniGame4 : ExecuteAfterNSecsBehavior
{

    public float HORIZONTAL_MOTION_SPEED = 0.5f;

    public Camera Camera;

    public GameObject needle;
    public GameObject connector;
    public GameObject needlePipe;
    public GameObject needleArmConnectorStart;
    public GameObject needleArmConnectorEnd;

    public GameObject needleHeartConnector;
    public GameObject heart;
    public GameObject needleHeartConnectorStart;
    public GameObject needleHeartConnectorEnd;

    public GameObject calmFace;
    public GameObject happyFace;
    public GameObject painFace;

    public GameObject donationArm;
    public GameObject donationArmTarget;

    public GameObject lightningBolts;
    public GameObject bolt0;
    public GameObject bolt1;
    public GameObject bolt2;

    public GameObject btnPushNeedle;

    public Sprite spriteConnectorFilled;
    public Sprite spriteNeedlePipeFilled;
    public Sprite spriteNeedleHeartConnectorFilled;
    public Sprite spriteHeartFilled;

    GameObjectsUtils mGameObjectUtils;
    bool mHorizontalConnectorHeadingRight;
    bool mVerticalConnectorHeadingDown;
    bool mHasNeedleTouchedTargetArm;

    float startPosNeedleConnector;
    float startPosNeedleTip;

    bool isPlayingVacineWrongHit;
    bool playingEndingSound = false;

    #region Initialization
    void Start()
    {
        GameController.LoadGame();
        InitGameVars();
        InitGameSound();
        InitGameStatus();
    }

    void InitGameVars()
    {
        MiniGameProgressController.miniGameStatus = 0;
        mGameObjectUtils = new GameObjectsUtils();
        mHorizontalConnectorHeadingRight = true;
        mVerticalConnectorHeadingDown = false;
        startPosNeedleConnector = connector.transform.position.y;
        startPosNeedleTip = needle.transform.position.y;
    }

    void InitGameSound()
    {
        SoundController.mgTransfusionStartGame = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vaccination_doctor_voice_game_start");
        SoundController.mgVaccinationHitCorrectly = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vaccination_hit_correctly");

        SoundController.mgTransfusionStartGame.start();

        isPlayingVacineWrongHit = false;
    }

    void InitGameStatus()
    {
        mHasNeedleTouchedTargetArm = false;
    }
    #endregion

    #region Update
    void Update()
    {

        if (!MiniGameProgressController.isMiniGamePaused)
        {
            UpdateStateGameObjects();
            ValidateGameLogic();
        }
    }

    void UpdateStateGameObjects()
    {

        if (mGameObjectUtils.ClickedGameObject(Camera, btnPushNeedle))
        {
            if (!mVerticalConnectorHeadingDown)
            {
                mVerticalConnectorHeadingDown = true;
            }
        }

        if (!mHasNeedleTouchedTargetArm) {
            MovePipeVertically();

            if (!mVerticalConnectorHeadingDown) {
                MovePipeHorizontally();
            }
        }

        mHasNeedleTouchedTargetArm = mGameObjectUtils.IsGameObjectAInsideB(needle.transform.position, donationArmTarget);
    }

    void MovePipeHorizontally()
    {

        if (mHorizontalConnectorHeadingRight)
        {
            // Update X position of the needle
            mGameObjectUtils.UpdateGameObjectX(needleHeartConnector, (float)(needleHeartConnector.transform.position.x + HORIZONTAL_MOTION_SPEED));
            UpdateNeedleXPosition(false, HORIZONTAL_MOTION_SPEED);

            if (needleHeartConnector.transform.position.x > needleHeartConnectorStart.transform.position.x)
            {
                mHorizontalConnectorHeadingRight = false;
            }

            // Update X position of the arm
            mGameObjectUtils.UpdateGameObjectX(donationArm, (float)(donationArm.transform.position.x - HORIZONTAL_MOTION_SPEED / 3));
        }
        else
        {
            // Update X position of the needle
            mGameObjectUtils.UpdateGameObjectX(needleHeartConnector, (float)(needleHeartConnector.transform.position.x - HORIZONTAL_MOTION_SPEED));
            UpdateNeedleXPosition(true, HORIZONTAL_MOTION_SPEED);

            if (needleHeartConnector.transform.position.x < needleHeartConnectorEnd.transform.position.x)
            {
                mHorizontalConnectorHeadingRight = true;
            }

            // Update X position of the arm
            mGameObjectUtils.UpdateGameObjectX(donationArm, (float)(donationArm.transform.position.x + HORIZONTAL_MOTION_SPEED / 3));
        }
    }

    void MovePipeVertically() {

        if (mVerticalConnectorHeadingDown) {

            if (connector.transform.position.y > needleArmConnectorEnd.transform.position.y)
            {
                UpdateNeedleYPosition(true, 0.2f);
            }
            else
            {

                Wait(0.1f, () => {

                    if (!mHasNeedleTouchedTargetArm)
                    {
                        ShowPainLightningBolts(true, needle.transform.position.x - (bolt0.transform.position.x - bolt1.transform.position.x));
                        PlaySoundWrongHit();
                        UpdateFaceHeart(2);
                    }
                });

                Wait(0.6f, () => {
                    ShowPainLightningBolts(false, needle.transform.position.x);
                    UpdateFaceHeart(1);

                    mGameObjectUtils.UpdateGameObjectY(needle, startPosNeedleTip);
                    mGameObjectUtils.UpdateGameObjectY(connector, startPosNeedleConnector);

                    mVerticalConnectorHeadingDown = false;
                });
            }
        }
    }

    void ShowPainLightningBolts(bool show, float centerXPos) {

        if (show) {
            mGameObjectUtils.UpdateGameObjectX(lightningBolts, centerXPos);

            bolt0.SetActive(true);
            bolt1.SetActive(true);
            bolt2.SetActive(true);

        } else {
            bolt0.SetActive(false);
            bolt1.SetActive(false);
            bolt2.SetActive(false);

            mGameObjectUtils.UpdateGameObjectX(lightningBolts, centerXPos);
        }
    }

    void PlaySoundWrongHit() {

        if (!isPlayingVacineWrongHit)
        {
            isPlayingVacineWrongHit = true;

            FMOD.Studio.EventInstance mgVaccinationWrongHit = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vaccination_wrong_hit_1");

            switch (Random.Range(1, 4))
            {

                case 1:
                    mgVaccinationWrongHit = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vaccination_wrong_hit_1");
                    break;

                case 2:
                    mgVaccinationWrongHit = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vaccination_wrong_hit_2");
                    break;

                case 3:
                    mgVaccinationWrongHit = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vaccination_wrong_hit_3");
                    break;
            }

            mgVaccinationWrongHit.start();

            Wait(2f, () =>
            {
                isPlayingVacineWrongHit = false;
            });
        }
    }

    void UpdateFaceHeart(int selectedFace) {

        switch (selectedFace) {

            case 1:
                calmFace.SetActive(true);
                painFace.SetActive(false);
                happyFace.SetActive(false);
                break;

            case 2:
                calmFace.SetActive(false);
                painFace.SetActive(true);
                happyFace.SetActive(false);
                break;

            case 3:
                calmFace.SetActive(false);
                painFace.SetActive(false);
                happyFace.SetActive(true);
                break;
        }
    }

    void UpdateNeedleXPosition(bool negative, float shift) {

        if (negative) {
            mGameObjectUtils.UpdateGameObjectX(needle, (float)(needle.transform.position.x - shift));
            mGameObjectUtils.UpdateGameObjectX(connector, (float)(connector.transform.position.x - shift));
            mGameObjectUtils.UpdateGameObjectX(needlePipe, (float)(needlePipe.transform.position.x - shift));

        } else {
            mGameObjectUtils.UpdateGameObjectX(needle, (float)(needle.transform.position.x + shift));
            mGameObjectUtils.UpdateGameObjectX(connector, (float)(connector.transform.position.x + shift));
            mGameObjectUtils.UpdateGameObjectX(needlePipe, (float)(needlePipe.transform.position.x + shift));
        }
    }

    void UpdateNeedleYPosition(bool negative, float shift)
    {

        if (negative)
        {
            mGameObjectUtils.UpdateGameObjectY(needle, (float)(needle.transform.position.y - shift));
            mGameObjectUtils.UpdateGameObjectY(connector, (float)(connector.transform.position.y - shift));
        }
        else
        {
            mGameObjectUtils.UpdateGameObjectY(needle, (float)(needle.transform.position.y + shift));
            mGameObjectUtils.UpdateGameObjectY(connector, (float)(connector.transform.position.y + shift));
        }
    }

    void ValidateGameLogic()
    {

        if (mHasNeedleTouchedTargetArm) {

            if (!playingEndingSound) {
                playingEndingSound = true;

                MiniGameProgressController.miniGameStatus = 1;

                ShowMiniGameEnding();

                Wait(2f, () => {
                    SoundController.StopSoundsMiniGame4();
                    SoundController.PlaySoundMiniGameWinRandom();

                    GameController.UpdateStatusMiniGameN(4, true);
                    MenuController.SOpenMiniGameNEnding(4, true);
                });
            }
        }
    }

    void ShowMiniGameEnding() {
        SoundController.mgVaccinationHitCorrectly.start();
        UpdateFaceHeart(3);
        FillPipesWithBlood();
    }

    void FillPipesWithBlood() {
        connector.GetComponent<SpriteRenderer>().sprite = spriteConnectorFilled;
        needlePipe.GetComponent<SpriteRenderer>().sprite = spriteNeedlePipeFilled;
        needleHeartConnector.GetComponent<SpriteRenderer>().sprite = spriteNeedleHeartConnectorFilled;
        heart.GetComponent<SpriteRenderer>().sprite = spriteHeartFilled;
    }
    #endregion

}