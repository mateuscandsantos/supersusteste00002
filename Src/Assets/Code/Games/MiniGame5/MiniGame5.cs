﻿using UnityEngine;

public class MiniGame5 : ExecuteAfterNSecsBehavior
{

    public float VERTICAL_MOTION_SPEED = 0.3f;
 
    public Camera Camera;

    public GameObject doctorArm;
    public GameObject doctorSeringe;
    public GameObject tipSeringe;

    public GameObject patient;
    public GameObject patientArm;
    public GameObject patientTarget;

    public GameObject lightningBolts;
    public GameObject bolt0;
    public GameObject bolt1;
    public GameObject bolt2;

    public GameObject verticalStart;
    public GameObject verticalEnd;
    public GameObject horizontalStart;
    public GameObject horizontalEnd;

    public GameObject btnPushNeedle;

    GameObjectsUtils mGameObjectUtils;
    bool mHasNeedleTouchedTargetArm;
    bool mIsTryingVaccinate;
    bool mVerticalMovHeadingUp;

    float startPosDoctorSeringe;
    float startPosDocArm;

    bool isPlayingVacineWrongHit = false;
    bool playingEndingSound = false;

    #region Initialization
    void Start()
    {
        GameController.LoadGame();
        InitGameVars();
        InitGameSound();
        InitGameStatus();
    }

    void InitGameVars()
    {
        MiniGameProgressController.miniGameStatus = 0;
        mGameObjectUtils = new GameObjectsUtils();
        mIsTryingVaccinate = false;
        mVerticalMovHeadingUp = true;
        startPosDoctorSeringe = doctorSeringe.transform.position.x;
        startPosDocArm = doctorArm.transform.position.x;
    }

    void InitGameSound()
    {
        SoundController.mgVaccinationStartGame = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vaccination_doctor_voice_game_start");
        SoundController.mgVaccinationHitCorrectly = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vaccination_hit_correctly");
        SoundController.mgVaccinationWrongHit1 = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vaccination_wrong_hit_1");
        SoundController.mgVaccinationWrongHit2 = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vaccination_wrong_hit_2");
        SoundController.mgVaccinationWrongHit3 = FMODUnity.RuntimeManager.CreateInstance("event:/mg_vaccination_wrong_hit_3");

        SoundController.mgVaccinationStartGame.start();
    }

    void InitGameStatus()
    {
        mHasNeedleTouchedTargetArm = false;
    }
    #endregion

    #region Update
    void Update()
    {

        if (!MiniGameProgressController.isMiniGamePaused)
        {
            UpdateStateGameObjects();
            ValidateGameLogic();
        }
    }

    private void UpdateStateGameObjects()
    {

        if (mGameObjectUtils.ClickedGameObject(Camera, btnPushNeedle))
        {
            if (!mIsTryingVaccinate)
            {
                mIsTryingVaccinate = true;
            }
        }

        if (!mHasNeedleTouchedTargetArm)
        {

            if (mIsTryingVaccinate)
            {
                MoveDocArmHorizontally();
            } else {
                MoveDocArmVertically();
            }
        }

        mHasNeedleTouchedTargetArm = mGameObjectUtils.IsGameObjectAInsideB(tipSeringe.transform.position, patientTarget);
    }

    void MoveDocArmVertically() {

        if (mVerticalMovHeadingUp)
        {
            // Update Y position of the seringe
            UpdateNeedleYPosition(false, VERTICAL_MOTION_SPEED);

            if (doctorSeringe.transform.position.y > verticalEnd.transform.position.y)
            {
                mVerticalMovHeadingUp = false;
            }

            // Update Y position of the patient arm
            mGameObjectUtils.UpdateGameObjectY(patient, (float)(patient.transform.position.y - VERTICAL_MOTION_SPEED / 4));
        }
        else
        {
            // Update Y position of the seringe
            UpdateNeedleYPosition(true, VERTICAL_MOTION_SPEED);

            if (doctorSeringe.transform.position.y < verticalStart.transform.position.y)
            {
                mVerticalMovHeadingUp = true;
            }

            // Update Y position of the patient arm
            mGameObjectUtils.UpdateGameObjectY(patient, (float)(patient.transform.position.y + VERTICAL_MOTION_SPEED / 4));
        }
    }

    void MoveDocArmHorizontally() {

        if (mIsTryingVaccinate)
        {

            if (doctorSeringe.transform.position.x < horizontalEnd.transform.position.x)
            {
                UpdateNeedleXPosition(false, 0.2f);
            }
            else
            {

                Wait(0.1f, () => {

                    if (!mHasNeedleTouchedTargetArm)
                    {
                        ShowPainLightningBolts(true, tipSeringe.transform.position.y - 3);
                        PlaySoundWrongHitRandom();
                    }
                });

                Wait(0.6f, () => {
                    ShowPainLightningBolts(false, patientTarget.transform.position.y);

                    mGameObjectUtils.UpdateGameObjectX(doctorSeringe, startPosDoctorSeringe);
                    mGameObjectUtils.UpdateGameObjectX(doctorArm, startPosDocArm);

                    mIsTryingVaccinate = false;
                });
            }
        }
    }

    public void PlaySoundWrongHitRandom()
    {
        int typeNumber = (int)Random.Range(1f, 3f);
        PlaySoundWrongHit(typeNumber);
    }

    public void PlaySoundWrongHit(int type)
    {

        if (!isPlayingVacineWrongHit)
        {
            isPlayingVacineWrongHit = true;

            switch (type)
            {

                case 1:
                    {
                        SoundController.mgVaccinationWrongHit1.start();
                        break;
                    }

                case 2:
                    {
                        SoundController.mgVaccinationWrongHit2.start();
                        break;
                    }

                case 3:
                    {
                        SoundController.mgVaccinationWrongHit3.start();
                        break;
                    }
            }

            Wait(2f, () =>
            {
                isPlayingVacineWrongHit = false;
            });
        }
    }

    void UpdateNeedleYPosition(bool negative, float shift)
    {

        if (negative)
        {
            mGameObjectUtils.UpdateGameObjectY(doctorSeringe, (float)(doctorSeringe.transform.position.y - shift));
            mGameObjectUtils.UpdateGameObjectY(doctorArm, (float)(doctorArm.transform.position.y - shift));

        }
        else
        {
            mGameObjectUtils.UpdateGameObjectY(doctorSeringe, (float)(doctorSeringe.transform.position.y + shift));
            mGameObjectUtils.UpdateGameObjectY(doctorArm, (float)(doctorArm.transform.position.y + shift));
        }
    }

    void UpdateNeedleXPosition(bool negative, float shift)
    {

        if (negative)
        {
            mGameObjectUtils.UpdateGameObjectX(doctorSeringe, (float)(doctorSeringe.transform.position.x - shift));
            mGameObjectUtils.UpdateGameObjectX(doctorArm, (float)(doctorArm.transform.position.x - shift));
        }
        else
        {
            mGameObjectUtils.UpdateGameObjectX(doctorSeringe, (float)(doctorSeringe.transform.position.x + shift));
            mGameObjectUtils.UpdateGameObjectX(doctorArm, (float)(doctorArm.transform.position.x + shift));
        }
    }

    void ShowPainLightningBolts(bool show, float centerYPos)
    {

        if (show)
        {
            mGameObjectUtils.UpdateGameObjectY(lightningBolts, centerYPos);

            bolt0.SetActive(true);
            bolt1.SetActive(true);
            bolt2.SetActive(true);
        }
        else
        {
            bolt0.SetActive(false);
            bolt1.SetActive(false);
            bolt2.SetActive(false);

            mGameObjectUtils.UpdateGameObjectY(lightningBolts, centerYPos);
        }
    }

    void ValidateGameLogic()
    {

        if (mHasNeedleTouchedTargetArm)
        {

            if (!playingEndingSound)
            {
                playingEndingSound = true;

                MiniGameProgressController.miniGameStatus = 1;

                ShowMiniGameEnding();

                Wait(2f, () =>
                {
                    SoundController.StopSoundsMiniGame5();
                    SoundController.PlaySoundMiniGameWinRandom();

                    GameController.UpdateStatusMiniGameN(5, true);
                    MenuController.SOpenMiniGameNEnding(5, true);
                });
            }
        }

    }

    void ShowMiniGameEnding()
    {
        SoundController.mgVaccinationHitCorrectly.start();
    }

    #endregion

}