﻿using UnityEngine;

public class MiniGame6 : ExecuteAfterNSecsBehavior {

    public float ROTATION_SPEED = 18f;
    public float SCALE_SPEED = 0.05f;

    public Camera Camera;
    public GameObject startCameraPos;
    public GameObject endCameraPos;
    public GameObject endCameraObj;

    public GameObject focus1;
    public GameObject focus2;
    public GameObject focus3;
    public GameObject focus4;
    public GameObject focus5;
    public GameObject focus6;
    public GameObject focus7;
    public GameObject focus8;
    public GameObject focus9;

    public GameObject explosionFocus1;
    public GameObject explosionFocus2;
    public GameObject explosionFocus3;
    public GameObject explosionFocus4;
    public GameObject explosionFocus5;
    public GameObject explosionFocus6;
    public GameObject explosionFocus7;
    public GameObject explosionFocus8;
    public GameObject explosionFocus9;

    GameObject[] focuses;
    GameObject[] focusesExplosion;
    bool[] focusIncreasing;

    Vector3 cameraVelocity = Vector3.zero;
    float smoothTime = 0.15f;

    GameObjectsUtils mGameObjUtils;

    bool playingSoundHitFocus = false;
    bool playingEndingSound = false;

    #region Initialization
    void Start()
    {
        GameController.LoadGame();
        InitGameVars();
        InitGameSound();
        InitGameStatus();
    }

    void InitGameVars()
    {
        MiniGameProgressController.miniGameStatus = 0;
        mGameObjUtils = new GameObjectsUtils();
    }

    void InitGameSound()
    {
        // ..
    }

    void InitGameStatus()
    {
        focus1.SetActive(true);
        focus2.SetActive(true);
        focus3.SetActive(true);
        focus4.SetActive(true);
        focus5.SetActive(true);
        focus6.SetActive(true);
        focus7.SetActive(true);
        focus8.SetActive(true);
        focus9.SetActive(true);

        focuses = new GameObject[9];
        focuses[0] = focus1;
        focuses[1] = focus2;
        focuses[2] = focus3;
        focuses[3] = focus4;
        focuses[4] = focus5;
        focuses[5] = focus6;
        focuses[6] = focus7;
        focuses[7] = focus8;
        focuses[8] = focus9;

        focusesExplosion = new GameObject[9];
        focusesExplosion[0] = explosionFocus1;
        focusesExplosion[1] = explosionFocus2;
        focusesExplosion[2] = explosionFocus3;
        focusesExplosion[3] = explosionFocus4;
        focusesExplosion[4] = explosionFocus5;
        focusesExplosion[5] = explosionFocus6;
        focusesExplosion[6] = explosionFocus7;
        focusesExplosion[7] = explosionFocus8;
        focusesExplosion[8] = explosionFocus9;

        focusIncreasing = new bool[9];
        focusIncreasing[0] = true;
        focusIncreasing[1] = true;
        focusIncreasing[2] = true;
        focusIncreasing[3] = true;
        focusIncreasing[4] = true;
        focusIncreasing[5] = true;
        focusIncreasing[6] = true;
        focusIncreasing[7] = true;
        focusIncreasing[8] = true;
    }
    #endregion

    #region Update
    void Update()
    {

        if (!MiniGameProgressController.isMiniGamePaused)
        {
            UpdateStateGameObjects();
            ValidateGameLogic();
        }
    }

    private void UpdateStateGameObjects()
    {
        IncreaseDecreaseSizeFocuses();
        DetectFocusExplosion();
        MoveCamera();
        DetectMissedFocus();
    }

    void IncreaseDecreaseSizeFocuses() {

        for (int i = 0; i < focuses.Length; i++)
        {
            Vector3 focusPosWithLeftPadding = new Vector3(focuses[i].transform.position.x,
                                                          focuses[i].transform.position.y,
                                                          focuses[i].transform.position.z);

            if (i > 2 && i < 5)
            {
                focusPosWithLeftPadding = new Vector3(focuses[i].transform.position.x - 7,
                                                      focuses[i].transform.position.y,
                                                      focuses[i].transform.position.z);
            }

            if (i >= 5)
            {
                focusPosWithLeftPadding = new Vector3(focuses[i].transform.position.x - 3,
                                                      focuses[i].transform.position.y,
                                                      focuses[i].transform.position.z);
            }

            Vector3 viewPos = Camera.WorldToViewportPoint(focusPosWithLeftPadding);

            // Only increase / decrease if the object is currently being focused 
            // by the camera
            if (viewPos.x > 0 && viewPos.x < 1)
            {

                if (focuses[i].transform.localScale.x < 1 && focusIncreasing[i])
                {
                    focuses[i].transform.localScale += new Vector3(SCALE_SPEED, SCALE_SPEED, 1f);
                    focuses[i].transform.Rotate(0, 0, ROTATION_SPEED, Space.World);
                }
            }
        }
    }

    void DetectMissedFocus() {

        for (int i = 0; i < focuses.Length; i++)
        {
            Vector3 viewPos = Camera.WorldToViewportPoint(focuses[i].transform.position);

            // If the camera has passed a focus, if it has already scaled up and the
            // user wasn't able to eliminate it, the game is over
            if (viewPos.x > 1 && focuses[i].transform.localScale.x >= 1 && focuses[i].activeSelf)
            {
                MiniGameProgressController.miniGameStatus = -1;
            }
        }
    }

    void DetectFocusExplosion() {

        for (int i = 0; i < focuses.Length; i++)
        {

            if (mGameObjUtils.ClickedGameObject(Camera, focuses[i]))
            {
                ExplodeObject(focuses[i], focusesExplosion[i]);
            }
        }
    }

    void ExplodeObject(GameObject obj, GameObject objExplosion)
    {
        // Explode sprite
        objExplosion.SetActive(true);

        Wait(0.1f, () => {
            obj.SetActive(false);
        });
        Wait(0.2f, () => {
            objExplosion.SetActive(false);
        });

        // Play the sound of the explosion
        if (!playingSoundHitFocus) {
            playingSoundHitFocus = true;

            FMOD.Studio.EventInstance explosionSound = FMODUnity.RuntimeManager.CreateInstance(SoundController.SOUND_EXPLOSION);
            explosionSound.start();

            Wait(0.5f, () =>
            {
                playingSoundHitFocus = false;
            });
        }
    }

    void MoveCamera()
    {
        float cameraXSize = endCameraObj.transform.position.x - startCameraPos.transform.position.x;
        float totalDeltaX = startCameraPos.transform.position.x - endCameraPos.transform.position.x;
        float currentDeltaX = totalDeltaX * ((100 - MiniGameProgressController.percentRemainingTime) * 0.0115f);

        Vector3 targetPos = new Vector3(startCameraPos.transform.position.x - currentDeltaX + (cameraXSize / 1.8f),
                                        Camera.transform.position.y,
                                        Camera.transform.position.z);

        Camera.transform.position = Vector3.SmoothDamp(Camera.transform.position,
                                                       targetPos,
                                                       ref cameraVelocity,
                                                       smoothTime);
    }

    void ValidateGameLogic()
    {

        if (!focus1.activeSelf && !focus2.activeSelf && !focus3.activeSelf &&
            !focus4.activeSelf && !focus5.activeSelf && !focus6.activeSelf &&
            !focus7.activeSelf && !focus8.activeSelf && !focus9.activeSelf)
        {

            if (!playingEndingSound)
            {
                playingEndingSound = true;

                MiniGameProgressController.miniGameStatus = 1;

                ShowMiniGameEnding();

                Wait(2f, () =>
                {
                    SoundController.StopSoundsMiniGame6();
                    SoundController.PlaySoundMiniGameWinRandom();

                    GameController.UpdateStatusMiniGameN(6, true);
                    MenuController.SOpenMiniGameNEnding(6, true);
                });
            }
        }

    }

    void ShowMiniGameEnding() {
        // ..
    }
    #endregion

}