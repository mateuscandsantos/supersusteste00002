﻿using UnityEngine;

public class MiniGame7 : ExecuteAfterNSecsBehavior
{

    public Camera Camera;

    public GameObject vertTrail0;
    public GameObject vertTrail1;
    public GameObject vertTrail2;
    public GameObject vertTrail3;
    public GameObject vertTrail4;

    public GameObject horTrail0;
    public GameObject horTrail1;
    public GameObject horTrail2;
    public GameObject horTrail3;
    public GameObject horTrail4;
    public GameObject horTrail5;

    public GameObject ocurrency0;
    public GameObject ocurrency1;
    public GameObject ocurrency2;
    public GameObject ocurrency3;
    public GameObject ocurrency4;
    public GameObject ocurrency5;
    public GameObject ocurrency0Solved;
    public GameObject ocurrency1Solved;
    public GameObject ocurrency2Solved;
    public GameObject ocurrency3Solved;
    public GameObject ocurrency4Solved;
    public GameObject ocurrency5Solved;

    public GameObject ambulance;
    public DragAndDropSpriteBehavior ambulanceSteeringWheel;
    public GameObject ambulanceUpSprite;
    public GameObject ambulanceDownSprite;
    public GameObject ambulanceLeftSprite;
    public GameObject ambulanceRightSprite;

    Vector3 latestValidAmbulancePos;
    Vector3 latestValidAmbulanceSteeringWheelPos;
    bool[] previousXMoveAmbulanceWasRight;
    bool[] previousYMoveAmbulanceWasUp;
    bool[] solvedOcurrencies;

    float offsetX = 0;
    float offsetY = 0;
    bool drifting = false;

    bool playingEndingSound = false;

    #region Initialization
    void Start()
    {
        GameController.LoadGame();
        InitGameVars();
        InitGameStatus();
        InitGameSound();
    }

    void InitGameVars()
    {
        // Trail related variables
        vertTrail0.GetComponent<BoxCollider2D>().isTrigger = true;
        vertTrail1.GetComponent<BoxCollider2D>().isTrigger = true;
        vertTrail2.GetComponent<BoxCollider2D>().isTrigger = true;
        vertTrail3.GetComponent<BoxCollider2D>().isTrigger = true;
        vertTrail4.GetComponent<BoxCollider2D>().isTrigger = true;

        horTrail0.GetComponent<BoxCollider2D>().isTrigger = true;
        horTrail1.GetComponent<BoxCollider2D>().isTrigger = true;
        horTrail2.GetComponent<BoxCollider2D>().isTrigger = true;
        horTrail3.GetComponent<BoxCollider2D>().isTrigger = true;
        horTrail4.GetComponent<BoxCollider2D>().isTrigger = true;
        horTrail5.GetComponent<BoxCollider2D>().isTrigger = true;

        vertTrail0.GetComponent<Rigidbody2D>().gravityScale = 0f;
        vertTrail1.GetComponent<Rigidbody2D>().gravityScale = 0f;
        vertTrail2.GetComponent<Rigidbody2D>().gravityScale = 0f;
        vertTrail3.GetComponent<Rigidbody2D>().gravityScale = 0f;
        vertTrail4.GetComponent<Rigidbody2D>().gravityScale = 0f;

        horTrail0.GetComponent<Rigidbody2D>().gravityScale = 0f;
        horTrail1.GetComponent<Rigidbody2D>().gravityScale = 0f;
        horTrail2.GetComponent<Rigidbody2D>().gravityScale = 0f;
        horTrail3.GetComponent<Rigidbody2D>().gravityScale = 0f;
        horTrail4.GetComponent<Rigidbody2D>().gravityScale = 0f;
        horTrail5.GetComponent<Rigidbody2D>().gravityScale = 0f;

        ocurrency0.GetComponent<BoxCollider2D>().isTrigger = true;
        ocurrency1.GetComponent<BoxCollider2D>().isTrigger = true;
        ocurrency2.GetComponent<BoxCollider2D>().isTrigger = true;
        ocurrency3.GetComponent<BoxCollider2D>().isTrigger = true;
        ocurrency4.GetComponent<BoxCollider2D>().isTrigger = true;
        ocurrency5.GetComponent<BoxCollider2D>().isTrigger = true;

        ocurrency0.GetComponent<Rigidbody2D>().gravityScale = 0f;
        ocurrency1.GetComponent<Rigidbody2D>().gravityScale = 0f;
        ocurrency2.GetComponent<Rigidbody2D>().gravityScale = 0f;
        ocurrency3.GetComponent<Rigidbody2D>().gravityScale = 0f;
        ocurrency4.GetComponent<Rigidbody2D>().gravityScale = 0f;
        ocurrency5.GetComponent<Rigidbody2D>().gravityScale = 0f;

        // Ambulance related variables
        ambulance.GetComponent<CircleCollider2D>().isTrigger = true;
        ambulance.GetComponent<Rigidbody2D>().gravityScale = 0f;

        offsetX = ambulanceSteeringWheel.transform.position.x - ambulance.transform.position.x;
        offsetY = ambulanceSteeringWheel.transform.position.y - ambulance.transform.position.y;

        latestValidAmbulancePos = ambulance.transform.position;
        latestValidAmbulanceSteeringWheelPos = ambulanceSteeringWheel.transform.position;

        previousXMoveAmbulanceWasRight = new bool[5];
        previousXMoveAmbulanceWasRight[0] = false;
        previousXMoveAmbulanceWasRight[1] = false;
        previousXMoveAmbulanceWasRight[2] = false;
        previousXMoveAmbulanceWasRight[3] = false;
        previousXMoveAmbulanceWasRight[4] = false;

        previousYMoveAmbulanceWasUp = new bool[5];
        previousYMoveAmbulanceWasUp[0] = true;
        previousYMoveAmbulanceWasUp[1] = true;
        previousYMoveAmbulanceWasUp[2] = true;
        previousYMoveAmbulanceWasUp[3] = true;
        previousYMoveAmbulanceWasUp[4] = true;

        ambulanceUpSprite.SetActive(true);
        ambulanceDownSprite.SetActive(false);
        ambulanceLeftSprite.SetActive(false);
        ambulanceRightSprite.SetActive(false);
    }

    void InitGameStatus()
    {
        MiniGameProgressController.miniGameStatus = 0;

        // Ocurrencies
        solvedOcurrencies = new bool[6];
        solvedOcurrencies[0] = false;
        solvedOcurrencies[1] = false;
        solvedOcurrencies[2] = false;
        solvedOcurrencies[3] = false;
        solvedOcurrencies[4] = false;
        solvedOcurrencies[5] = false;
    }

    void InitGameSound()
    {
        SoundController.mgSamuAtHomeRescue1 = FMODUnity.RuntimeManager.CreateInstance("event:/mg_samu_at_home_rescue_1");
        SoundController.mgSamuAtHomeRescue2 = FMODUnity.RuntimeManager.CreateInstance("event:/mg_samu_at_home_rescue_2");
        SoundController.mgSamuAtHomeRescue3 = FMODUnity.RuntimeManager.CreateInstance("event:/mg_samu_at_home_rescue_3");
        SoundController.mgSamuAtHomeDriving = FMODUnity.RuntimeManager.CreateInstance("event:/mg_samu_at_home_driving");
        SoundController.mgSamuAtHomeDrifting = FMODUnity.RuntimeManager.CreateInstance("event:/mg_samu_at_home_drift");

        SoundController.mgSamuAtHomeDriving.start();
    }
    #endregion

    #region Update
    void Update()
    {

        if (!MiniGameProgressController.isMiniGamePaused)
        {
            UpdateStateGameObjects();
            ValidateGameLogic();
        }
    }

    void UpdateStateGameObjects()
    {
        ambulance.transform.position = new Vector3(ambulanceSteeringWheel.transform.position.x + offsetX,
                                                   ambulanceSteeringWheel.transform.position.y + offsetY,
                                                   ambulanceSteeringWheel.transform.position.z);

        ColliderDistance2D vertTrailAmbulanceCollider0 = vertTrail0.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());
        ColliderDistance2D vertTrailAmbulanceCollider1 = vertTrail1.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());
        ColliderDistance2D vertTrailAmbulanceCollider2 = vertTrail2.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());
        ColliderDistance2D vertTrailAmbulanceCollider3 = vertTrail3.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());
        ColliderDistance2D vertTrailAmbulanceCollider4 = vertTrail4.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());

        ColliderDistance2D horTrailAmbulanceCollider0 = horTrail0.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());
        ColliderDistance2D horTrailAmbulanceCollider1 = horTrail1.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());
        ColliderDistance2D horTrailAmbulanceCollider2 = horTrail2.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());
        ColliderDistance2D horTrailAmbulanceCollider3 = horTrail3.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());
        ColliderDistance2D horTrailAmbulanceCollider4 = horTrail4.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());
        ColliderDistance2D horTrailAmbulanceCollider5 = horTrail5.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());

        // Determine the sprite of the ambulance that is showing, according to the
        // direction of the movement made by the user
        UpdateAmbulanceSpriteBasedOnDirection(latestValidAmbulancePos,
                                              ambulance.transform.position,
                                              vertTrailAmbulanceCollider0,
                                              vertTrailAmbulanceCollider1,
                                              vertTrailAmbulanceCollider2,
                                              vertTrailAmbulanceCollider3,
                                              vertTrailAmbulanceCollider4,
                                              horTrailAmbulanceCollider0,
                                              horTrailAmbulanceCollider1,
                                              horTrailAmbulanceCollider2,
                                              horTrailAmbulanceCollider3,
                                              horTrailAmbulanceCollider4,
                                              horTrailAmbulanceCollider5);

        // Ensure the ambulance is going to run only over the trails on the map
        UpdateAmbulancePosition(vertTrailAmbulanceCollider0,
                                vertTrailAmbulanceCollider1,
                                vertTrailAmbulanceCollider2,
                                vertTrailAmbulanceCollider3,
                                vertTrailAmbulanceCollider4,
                                horTrailAmbulanceCollider0,
                                horTrailAmbulanceCollider1,
                                horTrailAmbulanceCollider2,
                                horTrailAmbulanceCollider3,
                                horTrailAmbulanceCollider4,
                                horTrailAmbulanceCollider5);

        // Detect collisions between the ambulance and the ocurrencies
        DetectCollisionsBetweenAmbulanceOcurrencies();
    }

    #region AmbulanceSprite
    void UpdateAmbulanceSpriteBasedOnDirection(Vector3 latestPosition, 
                                               Vector3 updatedPosition,
                                               ColliderDistance2D vertTrailCollider0,
                                               ColliderDistance2D vertTrailCollider1,
                                               ColliderDistance2D vertTrailCollider2,
                                               ColliderDistance2D vertTrailCollider3,
                                               ColliderDistance2D vertTrailCollider4,
                                               ColliderDistance2D horTrailCollider0,
                                               ColliderDistance2D horTrailCollider1,
                                               ColliderDistance2D horTrailCollider2,
                                               ColliderDistance2D horTrailCollider3,
                                               ColliderDistance2D horTrailCollider4,
                                               ColliderDistance2D horTrailCollider5)
    {
        // Don't change the direction of the sprite when the ambulance is over
        // a crossroad
        bool isOverlappingVerticalTrail = vertTrailCollider0.isOverlapped || 
                                          vertTrailCollider1.isOverlapped ||
                                          vertTrailCollider2.isOverlapped || 
                                          vertTrailCollider3.isOverlapped ||
                                          vertTrailCollider4.isOverlapped;

        bool isOverlappingHorizontalTrail = horTrailCollider0.isOverlapped || 
                                            horTrailCollider1.isOverlapped ||
                                            horTrailCollider2.isOverlapped || 
                                            horTrailCollider3.isOverlapped ||
                                            horTrailCollider4.isOverlapped || 
                                            horTrailCollider5.isOverlapped;

        if (!(isOverlappingVerticalTrail && isOverlappingHorizontalTrail) &&
            isOverlappingVerticalTrail || isOverlappingHorizontalTrail)
        {
            // Detect vertical movement
            if (isOverlappingVerticalTrail)
            {
                ambulanceLeftSprite.SetActive(false);
                ambulanceRightSprite.SetActive(false);

                UpdateAmbulanceSpriteOnVerticalAxis(latestPosition, updatedPosition);
            }
            // Detect horizontal movement
            else if (isOverlappingHorizontalTrail)
            {
                ambulanceUpSprite.SetActive(false);
                ambulanceDownSprite.SetActive(false);

                UpdateAmbulanceSpriteOnHorizontalAxis(latestPosition, updatedPosition);
            }
        }
    }

    void UpdateAmbulanceSpriteOnVerticalAxis(Vector3 latestPosition, Vector3 updatedPosition) {
        float deltaY = updatedPosition.y - latestPosition.y;

        // Set upwards sprite and avoid jittering on movements with slow speeds
        if (deltaY > 0)
        {

            if (MovingUpwards())
            {
                ambulanceUpSprite.SetActive(true);
                ambulanceDownSprite.SetActive(false);
            }
            else
            {
                UpdateMovementUpwards(true);
            }
        }
        // Set downwards sprite and avoid jittering on movements with slow speeds
        else if (deltaY < 0)
        {

            if (!MovingUpwards())
            {
                ambulanceUpSprite.SetActive(false);
                ambulanceDownSprite.SetActive(true);
            }
            else
            {
                UpdateMovementUpwards(false);
            }
        }
    }

    void UpdateAmbulanceSpriteOnHorizontalAxis(Vector3 latestPosition, Vector3 updatedPosition)
    {
        float deltaX = updatedPosition.x - latestPosition.x;

        // Set rightwards sprite and avoid jittering on movements with slow speeds
        if (deltaX > 0)
        {

            if (MovingRightwards())
            {
                ambulanceRightSprite.SetActive(true);
                ambulanceLeftSprite.SetActive(false);
            }
            else
            {
                UpdateMovementRightwards(true);
            }
        }
        // Set leftwards sprite and avoid jittering on movements with slow speeds
        else if (deltaX < 0) 
        {

            if (!MovingRightwards())
            {
                ambulanceRightSprite.SetActive(false);
                ambulanceLeftSprite.SetActive(true);
            }
            else
            {
                UpdateMovementRightwards(false);
            }
        }
    }

    bool MovingUpwards()
    {
        bool movedUp = true;

        foreach (bool prevMovedUp in previousYMoveAmbulanceWasUp)
        {

            if (!prevMovedUp)
            {
                movedUp = false;
                break;
            }
        }

        return movedUp;
    }

    bool MovingRightwards()
    {
        bool movedRight = true;

        foreach (bool prevMovedRight in previousXMoveAmbulanceWasRight)
        {

            if (!prevMovedRight)
            {
                movedRight = false;
                break;
            }
        }

        return movedRight;
    }

    void UpdateMovementUpwards(bool up) 
    {

        for (int i = 0; i < previousYMoveAmbulanceWasUp.Length; i++)
        {

            if (up)
            {

                if (!previousYMoveAmbulanceWasUp[i])
                {
                    previousYMoveAmbulanceWasUp[i] = true;
                    break;
                }
            }
            else
            {

                if (previousYMoveAmbulanceWasUp[i])
                {
                    previousYMoveAmbulanceWasUp[i] = false;
                    break;
                }
            }
        }
    }

    void UpdateMovementRightwards(bool right)
    {

        for (int i = 0; i < previousXMoveAmbulanceWasRight.Length; i++)
        {

            if (right) {

                if (!previousXMoveAmbulanceWasRight[i])
                {
                    previousXMoveAmbulanceWasRight[i] = true;
                    break;
                }
            } else {

                if (previousXMoveAmbulanceWasRight[i])
                {
                    previousXMoveAmbulanceWasRight[i] = false;
                    break;
                }
            }
        }
    }
    #endregion

    #region AmbulanceDirection
    void UpdateAmbulancePosition(ColliderDistance2D ambulanceTrailCollider1,
                                 ColliderDistance2D ambulanceTrailCollider2,
                                 ColliderDistance2D ambulanceTrailCollider3,
                                 ColliderDistance2D ambulanceTrailCollider4,
                                 ColliderDistance2D ambulanceTrailCollider6,
                                 ColliderDistance2D ambulanceTrailCollider7,
                                 ColliderDistance2D ambulanceTrailCollider8,
                                 ColliderDistance2D ambulanceTrailCollider9,
                                 ColliderDistance2D ambulanceTrailCollider10,
                                 ColliderDistance2D ambulanceTrailCollider11,
                                 ColliderDistance2D ambulanceTrailCollider12) {

        if (ambulanceTrailCollider1.isOverlapped ||
            ambulanceTrailCollider2.isOverlapped ||
            ambulanceTrailCollider3.isOverlapped ||
            ambulanceTrailCollider4.isOverlapped ||
            ambulanceTrailCollider6.isOverlapped ||
            ambulanceTrailCollider7.isOverlapped ||
            ambulanceTrailCollider8.isOverlapped ||
            ambulanceTrailCollider9.isOverlapped ||
            ambulanceTrailCollider10.isOverlapped ||
            ambulanceTrailCollider11.isOverlapped ||
            ambulanceTrailCollider12.isOverlapped)
        {
            latestValidAmbulancePos = ambulance.transform.position;
            latestValidAmbulanceSteeringWheelPos = ambulanceSteeringWheel.transform.position;
        }
        else
        {
            ambulance.transform.position = latestValidAmbulancePos;
            ambulanceSteeringWheel.transform.position = latestValidAmbulanceSteeringWheelPos;

            if (!drifting) {
                drifting = true;

                Wait(2f, () =>
                {
                    SoundController.mgSamuAtHomeDrifting.start();
                    ambulance.transform.Rotate(0, 360f, 0, Space.World);
                });
            }
        }
    }
    #endregion

    #region Ocurrencies
    void DetectCollisionsBetweenAmbulanceOcurrencies() {
        ColliderDistance2D ocurrencyAmbulanceCollider0 = ocurrency0.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());
        ColliderDistance2D ocurrencyAmbulanceCollider1 = ocurrency1.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());
        ColliderDistance2D ocurrencyAmbulanceCollider2 = ocurrency2.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());
        ColliderDistance2D ocurrencyAmbulanceCollider3 = ocurrency3.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());
        ColliderDistance2D ocurrencyAmbulanceCollider4 = ocurrency4.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());
        ColliderDistance2D ocurrencyAmbulanceCollider5 = ocurrency5.GetComponent<BoxCollider2D>().Distance(ambulance.GetComponent<CircleCollider2D>());

        if (ocurrencyAmbulanceCollider0.isOverlapped && !solvedOcurrencies[0]) {
            solvedOcurrencies[0] = true;
            SolveOcurrency(ocurrency0, ocurrency0Solved);
        }

        if (ocurrencyAmbulanceCollider1.isOverlapped && !solvedOcurrencies[1])
        {
            solvedOcurrencies[1] = true;
            SolveOcurrency(ocurrency1, ocurrency1Solved);
        }

        if (ocurrencyAmbulanceCollider2.isOverlapped && !solvedOcurrencies[2])
        {
            solvedOcurrencies[2] = true;
            SolveOcurrency(ocurrency2, ocurrency2Solved);
        }

        if (ocurrencyAmbulanceCollider3.isOverlapped && !solvedOcurrencies[3])
        {
            solvedOcurrencies[3] = true;
            SolveOcurrency(ocurrency3, ocurrency3Solved);
        }

        if (ocurrencyAmbulanceCollider4.isOverlapped && !solvedOcurrencies[4])
        {
            solvedOcurrencies[4] = true;
            SolveOcurrency(ocurrency4, ocurrency4Solved);
        }

        if (ocurrencyAmbulanceCollider5.isOverlapped && !solvedOcurrencies[5])
        {
            solvedOcurrencies[5] = true;
            SolveOcurrency(ocurrency5, ocurrency5Solved);
        }
    }

    void SolveOcurrency(GameObject ocurrencyUnsolved, GameObject ocurrencySolved) {
        // Play a sound
        SoundController.PlaySoundRescueVictimRandom();

        // Animation that flips the unsolved ocurrency
        ocurrencyUnsolved.SetActive(false);
        ocurrencySolved.SetActive(true);
    }
    #endregion

    void ValidateGameLogic()
    {

        if (SolvedAllOcurrencies()) {

            if (!playingEndingSound)
            {
                playingEndingSound = true;

                MiniGameProgressController.miniGameStatus = 1;

                ShowMiniGameEnding();

                Wait(2f, () =>
                {
                    SoundController.StopSoundsMiniGame7();
                    SoundController.PlaySoundMiniGameWinRandom();

                    GameController.UpdateStatusMiniGameN(7, true);
                    MenuController.SOpenMiniGameNEnding(7, true);
                });
            }
        }
    }

    bool SolvedAllOcurrencies() {
        bool resultSolvedAllOcurrencies = true;

        foreach (bool ocurrency in solvedOcurrencies) {

            if (!ocurrency) {
                resultSolvedAllOcurrencies = false;
                break;
            }
        }

        return resultSolvedAllOcurrencies;
    }

    void ShowMiniGameEnding() {
        // ..
    }
    #endregion

}