﻿using UnityEngine;

public class MiniGame8 : ExecuteAfterNSecsBehavior
{

    public Camera Camera;

    bool playingEndingSound = false;

    #region Initialization
    void Start()
    {
        GameController.LoadGame();
        InitGameVars();
        InitGameStatus();
        InitGameSound();
    }

    void InitGameVars()
    {
        // ..
    }

    void InitGameStatus()
    {
        MiniGameProgressController.miniGameStatus = 0;

        // ..
    }

    void InitGameSound()
    {
        //SoundController.mgSamuAtHomeRescue1 = FMODUnity.RuntimeManager.CreateInstance("event:/mg_samu_at_home_rescue_1");
    }
    #endregion

    #region Update
    void Update()
    {

        if (!MiniGameProgressController.isMiniGamePaused)
        {
            UpdateStateGameObjects();
            ValidateGameLogic();
        }
    }

    void UpdateStateGameObjects()
    {
        // ..
    }
    #endregion

    #region
    void ValidateGameLogic()
    {

        if (FinishedGame())
        {

            if (!playingEndingSound)
            {
                playingEndingSound = true;

                MiniGameProgressController.miniGameStatus = 1;

                ShowMiniGameEnding();

                Wait(2f, () =>
                {
                    SoundController.StopSoundsMiniGame8();
                    SoundController.PlaySoundMiniGameWinRandom();

                    GameController.UpdateStatusMiniGameN(8, true);
                    MenuController.SOpenMiniGameNEnding(8, true);
                });
            }
        }
    }

    bool FinishedGame()
    {
        bool resultSolvedAllOcurrencies = true;

        /*
        foreach (bool ocurrency in solvedOcurrencies)
        {

            if (!ocurrency)
            {
                resultSolvedAllOcurrencies = false;
                break;
            }
        }
        */

        return resultSolvedAllOcurrencies;
    }

    void ShowMiniGameEnding()
    {
        // ..
    }
    #endregion

}
