﻿using UnityEngine;

public class AboutPage : MonoBehaviour {

    void Start()
    {
        GameController.LoadGame();
    }

    public void OpenCitizensLetter() 
    {
		Application.OpenURL ("http://bvsms.saude.gov.br/bvs/publicacoes/cartas_direitos_usuarios_saude_3ed.pdf");
	}

    public void OpenGpsstecSite()
    {
        Application.OpenURL("http://eagoracidadao.fiocruz.br/");
    }

}