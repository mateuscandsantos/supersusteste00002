﻿using UnityEngine;
using UnityEngine.UI;

public class AchievementsPage : MonoBehaviour {

    public Text lblLevel;
    public Text lblNumBadges;
    public Text lblNumYearsLife;
    public Text lblTotalCompletionTime;

    public Sprite spriteBadge1Ok;
    public Sprite spriteBadge1NotOk;
    public Sprite spriteBadge2Ok;
    public Sprite spriteBadge2NotOk;
    public Sprite spriteBadge3Ok;
    public Sprite spriteBadge3NotOk;
    public Sprite spriteBadge4Ok;
    public Sprite spriteBadge4NotOk;
    public Sprite spriteBadge5Ok;
    public Sprite spriteBadge5NotOk;
    public Sprite spriteBadge6Ok;
    public Sprite spriteBadge6NotOk;
    public Sprite spriteBadge7Ok;
    public Sprite spriteBadge7NotOk;
    public Sprite spriteBadge8Ok;
    public Sprite spriteBadge8NotOk;
    public Sprite spriteBadge9Ok;
    public Sprite spriteBadge9NotOk;
    public Sprite spriteBadge10Ok;
    public Sprite spriteBadge10NotOk;
    public Sprite spriteBadge11Ok;
    public Sprite spriteBadge11NotOk;
    public Sprite spriteBadge12Ok;
    public Sprite spriteBadge12NotOk;
    public Sprite spriteBadge13Ok;
    public Sprite spriteBadge13NotOk;
    public Sprite spriteBadge14Ok;
    public Sprite spriteBadge14NotOk;
    public Sprite spriteBadge15Ok;
    public Sprite spriteBadge15NotOk;
    public Sprite spriteBadge16Ok;
    public Sprite spriteBadge16NotOk;
    public Sprite spriteBadge17Ok;
    public Sprite spriteBadge17NotOk;
    public Sprite spriteBadge18Ok;
    public Sprite spriteBadge18NotOk;
    public Sprite spriteBadge19Ok;
    public Sprite spriteBadge19NotOk;
    public Sprite spriteBadge20Ok;
    public Sprite spriteBadge20NotOk;
    public Sprite spriteBadge21Ok;
    public Sprite spriteBadge21NotOk;

    public Image imgBadge1;
    public Image imgBadge2;
    public Image imgBadge3;
    public Image imgBadge4;
    public Image imgBadge5;
    public Image imgBadge6;
    public Image imgBadge7;
    public Image imgBadge8;
    public Image imgBadge9;
    public Image imgBadge10;
    public Image imgBadge11;
    public Image imgBadge12;
    public Image imgBadge13;
    public Image imgBadge14;
    public Image imgBadge15;
    public Image imgBadge16;
    public Image imgBadge17;
    public Image imgBadge18;
    public Image imgBadge19;
    public Image imgBadge20;
    public Image imgBadge21;

    private void Start()
    {
        GameController.LoadGame();
        LoadAchievements();
    }

    public void LoadAchievements() 
    {
        Debug.Log(GameController.player);

        lblLevel.text = GameController.player.GetLevel().ToString();
        lblNumBadges.text = GameController.player.GetNumBadges().ToString();
        lblNumYearsLife.text = GameController.player.GetNumYearsLife().ToString();
        lblTotalCompletionTime.text = GameController.player.GetGameTotalCompletionTime().ToString();
        LoadBadges();
    }

    private void LoadBadges() 
    {
        if (GameController.player.HasBadge(1))
        {
            imgBadge1.sprite = spriteBadge1Ok;
        }
        else
        {
            imgBadge1.sprite = spriteBadge1NotOk;
        }

        if (GameController.player.HasBadge(2))
        {
            imgBadge2.sprite = spriteBadge2Ok;
        }
        else
        {
            imgBadge2.sprite = spriteBadge2NotOk;
        }

        if (GameController.player.HasBadge(3))
        {
            imgBadge3.sprite = spriteBadge3Ok;
        }
        else
        {
            imgBadge3.sprite = spriteBadge3NotOk;
        }

        if (GameController.player.HasBadge(4))
        {
            imgBadge4.sprite = spriteBadge4Ok;
        }
        else
        {
            imgBadge4.sprite = spriteBadge4NotOk;
        }

        if (GameController.player.HasBadge(5))
        {
            imgBadge5.sprite = spriteBadge5Ok;
        }
        else
        {
            imgBadge5.sprite = spriteBadge5NotOk;
        }

        if (GameController.player.HasBadge(6))
        {
            imgBadge6.sprite = spriteBadge6Ok;
        }
        else
        {
            imgBadge6.sprite = spriteBadge6NotOk;
        }

        if (GameController.player.HasBadge(7))
        {
            imgBadge7.sprite = spriteBadge7Ok;
        }
        else
        {
            imgBadge7.sprite = spriteBadge7NotOk;
        }

        if (GameController.player.HasBadge(8))
        {
            imgBadge8.sprite = spriteBadge8Ok;
        }
        else
        {
            imgBadge8.sprite = spriteBadge8NotOk;
        }

        if (GameController.player.HasBadge(9))
        {
            imgBadge9.sprite = spriteBadge9Ok;
        }
        else
        {
            imgBadge9.sprite = spriteBadge9NotOk;
        }

        if (GameController.player.HasBadge(10))
        {
            imgBadge10.sprite = spriteBadge10Ok;
        }
        else
        {
            imgBadge10.sprite = spriteBadge10NotOk;
        }

        if (GameController.player.HasBadge(11))
        {
            imgBadge11.sprite = spriteBadge11Ok;
        }
        else
        {
            imgBadge11.sprite = spriteBadge11NotOk;
        }

        if (GameController.player.HasBadge(12))
        {
            imgBadge12.sprite = spriteBadge12Ok;
        }
        else
        {
            imgBadge12.sprite = spriteBadge12NotOk;
        }

        if (GameController.player.HasBadge(13))
        {
            imgBadge13.sprite = spriteBadge13Ok;
        }
        else
        {
            imgBadge13.sprite = spriteBadge13NotOk;
        }

        if (GameController.player.HasBadge(14))
        {
            imgBadge14.sprite = spriteBadge14Ok;
        }
        else
        {
            imgBadge14.sprite = spriteBadge14NotOk;
        }

        if (GameController.player.HasBadge(15))
        {
            imgBadge15.sprite = spriteBadge15Ok;
        }
        else
        {
            imgBadge15.sprite = spriteBadge15NotOk;
        }

        if (GameController.player.HasBadge(16))
        {
            imgBadge16.sprite = spriteBadge16Ok;
        }
        else
        {
            imgBadge16.sprite = spriteBadge16NotOk;
        }

        if (GameController.player.HasBadge(17))
        {
            imgBadge17.sprite = spriteBadge17Ok;
        }
        else
        {
            imgBadge17.sprite = spriteBadge17NotOk;
        }

        if (GameController.player.HasBadge(18))
        {
            imgBadge18.sprite = spriteBadge18Ok;
        }
        else
        {
            imgBadge18.sprite = spriteBadge18NotOk;
        }

        if (GameController.player.HasBadge(19))
        {
            imgBadge19.sprite = spriteBadge19Ok;
        }
        else
        {
            imgBadge19.sprite = spriteBadge19NotOk;
        }

        if (GameController.player.HasBadge(20))
        {
            imgBadge20.sprite = spriteBadge20Ok;
        }
        else
        {
            imgBadge20.sprite = spriteBadge20NotOk;
        }

        if (GameController.player.HasBadge(21))
        {
            imgBadge21.sprite = spriteBadge21Ok;
        }
        else
        {
            imgBadge21.sprite = spriteBadge21NotOk;
        }
    }

    public void OpenLatestGame () 
    {
        Debug.Log("Recover state and navigate to the latest game the user played.");
    }

}