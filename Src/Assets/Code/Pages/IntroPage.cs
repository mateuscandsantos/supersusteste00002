﻿using UnityEngine;

public class IntroPage : MonoBehaviour {

    void Start() {
        GameController.LoadGame();
    }

    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            SoundController.SPlaySoundBtnClick();
            MenuController.SOpenLevels();
        }
    }

}