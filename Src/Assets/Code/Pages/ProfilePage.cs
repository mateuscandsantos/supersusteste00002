﻿using UnityEngine;
using UnityEngine.UI;

public class ProfilePage : MonoBehaviour
{

    public InputField playerName;
    public InputField playerAge;
    public GlobalPlayerController globalPlayer;

    #region Initialization
    private void Start()
    {
        GameController.LoadGame();
        GlobalPlayerController.isMovementLocked = true;
        LoadUserData();
    }

    void LoadUserData() {
        playerName.text = GameController.player.GetName();
        playerAge.text = GameController.player.GetAge().ToString();
    }
    #endregion

    #region UIEvents
    public void OnClickHairSelector(bool pressedLeftBtn)
    {
        globalPlayer.SelectHair(pressedLeftBtn);
        globalPlayer.SaveAppearance(playerName.text, playerAge.text);
    }

    public void OnClickEyesSelector(bool pressedLeftBtn)
    {
        globalPlayer.SelectEyes(pressedLeftBtn);
        globalPlayer.SaveAppearance(playerName.text, playerAge.text);
    }

    public void OnClickTShirtSelector(bool pressedLeftBtn)
    {
        globalPlayer.SelectTShirt(pressedLeftBtn);
        globalPlayer.SaveAppearance(playerName.text, playerAge.text);
    }

    public void OnClickPantsSelector(bool pressedLeftBtn)
    {
        globalPlayer.SelectPants(pressedLeftBtn);
        globalPlayer.SaveAppearance(playerName.text, playerAge.text);
    }
    #endregion

}