﻿using UnityEngine;
using UnityEngine.UI;

public class SettingsPage : MonoBehaviour
{

    public float SOUND_DELTA_VALUE_PER_CLICK = 0.1f;

    public Button btnSoundEffects;
    public Button btnSoundTheme;
    public Button btnSoundEffectsUp;
    public Button btnSoundEffectsDown;
    public Button btnSoundThemeUp;
    public Button btnSoundThemeDown;

    public GameObject settingsTabSoundEffects;
    public GameObject settingsTabSound;

    public Toggle enableSound;

    public Slider soundEffectsLevelSlider;
    public Slider soundLevelSlider;

    float mPreviousSoundLevel;
    float mPreviousSoundEffectsLevel;

    #region Initialization
    void Start()
    {
        GameController.LoadGame();
        LoadSettings();
        InitUI();
    }

    void LoadSettings()
    {
        enableSound.isOn = GameController.settings.IsSoundEnabled();

        soundLevelSlider.enabled = enableSound.isOn;
        soundLevelSlider.value = GameController.settings.GetSoundLevel();
        mPreviousSoundLevel = GameController.settings.GetSoundLevel();

        soundEffectsLevelSlider.enabled = enableSound.isOn;
        soundEffectsLevelSlider.value = GameController.settings.GetSoundEffectsLevel();
        mPreviousSoundEffectsLevel = GameController.settings.GetSoundEffectsLevel();
    }

    void InitUI() {
        btnSoundEffects.interactable = false;
        btnSoundTheme.interactable = true;
    }
    #endregion

    #region Loop
    void Update()
    {
        UpdateSoundAndEffectsValues();
    }

    private void UpdateSoundAndEffectsValues() 
    {
        // Update the overall sound status
        if (enableSound.isOn)
        {
            FMODUnity.RuntimeManager.MuteAllEvents(false);
            SoundController.MuteAllInstances(false);
        }
        else
        {
            FMODUnity.RuntimeManager.MuteAllEvents(true);
            SoundController.MuteAllInstances(true);
        }

        // Update the sound setting
        if (!enableSound.isOn) 
        {
            soundLevelSlider.value = mPreviousSoundLevel;
            soundLevelSlider.enabled = false;
        } 
        else if (!MathUtils.IsEqual(mPreviousSoundLevel, soundLevelSlider.value))
        {
            mPreviousSoundLevel = soundLevelSlider.value;

            soundLevelSlider.enabled = true;

            GameController.settings.SetSoundLevel(soundLevelSlider.value);
            SoundController.UpdateThemeVolume();
        }

        // Update the sound effects setting
        if (!enableSound.isOn)
        {
            soundEffectsLevelSlider.value = mPreviousSoundEffectsLevel;
            soundEffectsLevelSlider.enabled = false;
        }
        else if (!MathUtils.IsEqual(mPreviousSoundEffectsLevel, soundEffectsLevelSlider.value))
        {
            mPreviousSoundEffectsLevel = soundEffectsLevelSlider.value;

            soundEffectsLevelSlider.enabled = true;

            GameController.settings.SetSoundEffectsLevel(soundEffectsLevelSlider.value);
            SoundController.UpdateSoundEffectsVolume();
        }
    }
    #endregion

    #region UIEvents
    public void OnClickSettingsOpt(string selectedSetting) {

        switch (selectedSetting) {

            case "soundEffects":
                settingsTabSound.SetActive(false);
                settingsTabSoundEffects.SetActive(true);

                btnSoundEffects.interactable = false;
                btnSoundTheme.interactable = true;

                break;

            case "sound":
                settingsTabSound.SetActive(true);
                settingsTabSoundEffects.SetActive(false);

                btnSoundEffects.interactable = true;
                btnSoundTheme.interactable = false;

                break;
        }
    }

    public void OnClickTurnSoundEffectsUp(bool turnUp) {

        if (!btnSoundEffects.interactable) 
        {

            if (turnUp)
            {

                if (soundEffectsLevelSlider.value + SOUND_DELTA_VALUE_PER_CLICK > 1)
                {
                    soundEffectsLevelSlider.value = 1;

                }
                else
                {
                    soundEffectsLevelSlider.value = soundEffectsLevelSlider.value + SOUND_DELTA_VALUE_PER_CLICK;
                }
            }
            else
            {

                if (soundEffectsLevelSlider.value - SOUND_DELTA_VALUE_PER_CLICK < 0)
                {
                    soundEffectsLevelSlider.value = 0;
                }
                else
                {
                    soundEffectsLevelSlider.value = soundEffectsLevelSlider.value - SOUND_DELTA_VALUE_PER_CLICK;
                }
            }

            GameController.settings.SetSoundEffectsLevel(soundEffectsLevelSlider.value);
            SoundController.UpdateSoundEffectsVolume();
            SoundController.SPlaySoundBtnClick();
        }
    }

    public void OnClickTurnSoundThemeUp(bool turnUp)
    {

        if (!btnSoundTheme.interactable)
        {

            if (turnUp)
            {

                if (soundLevelSlider.value + SOUND_DELTA_VALUE_PER_CLICK > 1)
                {
                    soundLevelSlider.value = 1;

                }
                else
                {
                    soundLevelSlider.value = soundLevelSlider.value + SOUND_DELTA_VALUE_PER_CLICK;
                }
            }
            else
            {

                if (soundLevelSlider.value - SOUND_DELTA_VALUE_PER_CLICK < 0)
                {
                    soundLevelSlider.value = 0;
                }
                else
                {
                    soundLevelSlider.value = soundLevelSlider.value - SOUND_DELTA_VALUE_PER_CLICK;
                }
            }

            GameController.settings.SetSoundLevel(soundLevelSlider.value);
            SoundController.UpdateThemeVolume();
            SoundController.SPlaySoundBtnClick();
        }
    }

    public void OnClickSaveBtn()
    {
        GameController.UpdateSettings();
        SoundController.SPlaySoundBtnClick();
        MenuController.SOpenLevels();
    }
    #endregion

}