﻿using UnityEngine;

public class SplashPage : ExecuteAfterNSecsBehavior {

    void Start()
    {
        GameController.LoadGame();
        SoundController.InitSound();
        SoundController.PlayTheme();

        Wait(1, () => {
            MenuController.SOpenIntro();
        });
    }

}