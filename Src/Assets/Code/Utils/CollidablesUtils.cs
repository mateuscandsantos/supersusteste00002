﻿public class CollidablesUtils
{

    public static readonly string TAG_WALL = "wall";
    public static readonly string TAG_SPIKE = "spike";
    public static readonly string TAG_GAME_1 = "game_1";
    public static readonly string TAG_MINI_GAME_1 = "mini_game_1";
    public static readonly string TAG_MINI_GAME_2 = "mini_game_2";
    public static readonly string TAG_MINI_GAME_3 = "mini_game_3";
    public static readonly string TAG_MINI_GAME_4 = "mini_game_4";
    public static readonly string TAG_MINI_GAME_5 = "mini_game_5";
    public static readonly string TAG_MINI_GAME_6 = "mini_game_6";
    public static readonly string TAG_MINI_GAME_7 = "mini_game_7";
    public static readonly string TAG_MINI_GAME_8 = "mini_game_8";
    public static readonly string TAG_MINI_GAME_9 = "mini_game_9";
    public static readonly string TAG_MINI_GAME_10 = "mini_game_10";
    public static readonly string TAG_MINI_GAME_11 = "mini_game_11";
    public static readonly string TAG_MINI_GAME_12 = "mini_game_12";
    public static readonly string TAG_MINI_GAME_13 = "mini_game_13";
    public static readonly string TAG_MINI_GAME_14 = "mini_game_14";
    public static readonly string TAG_MINI_GAME_15 = "mini_game_15";
    public static readonly string TAG_MINI_GAME_16 = "mini_game_16";
    public static readonly string TAG_MINI_GAME_17 = "mini_game_17";
    public static readonly string TAG_MINI_GAME_18 = "mini_game_18";
    public static readonly string TAG_MINI_GAME_19 = "mini_game_19";
    public static readonly string TAG_MINI_GAME_20 = "mini_game_20";
    public static readonly string TAG_MINI_GAME_21 = "mini_game_21";
    public static readonly string TAG_CHECK_LEVEL_1 = "check_level_1";
    public static readonly string TAG_CHECK_LEVEL_2 = "check_level_2";
    public static readonly string TAG_CHECK_LEVEL_3 = "check_level_3";
    public static readonly string TAG_MINI_GAME_7_MAP_TRAIL_TILE = "mg_samu_at_home_ground_tile";

}