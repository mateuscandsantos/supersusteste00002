﻿using UnityEngine;

public class DragAndDropSpriteBehavior : MonoBehaviour
{
    public Camera Camera;

    public bool isDragging = false;
    public bool isDraggable = true;

    void Update()
    {

        if (Camera != null && isDraggable)
        {

            if (!MiniGameProgressController.isMiniGamePaused)
            {
                DragObject();
            }
        }
    }

    void DragObject() {
        // Gets the world position of the mouse on the screen        
        Vector2 mousePosition2D = Camera.ScreenToWorldPoint(Input.mousePosition);
        Vector3 mousePosition3D = new Vector3(mousePosition2D.x,
                                              mousePosition2D.y,
                                              GetComponent<SpriteRenderer>().bounds.center.z);

        // Checks whether the mouse is over the sprite 3D area and if the user 
        // is pressing the mouse button / touching the screen
        if (GetComponent<SpriteRenderer>().bounds.Contains(mousePosition3D) && Input.GetButton("Fire1"))
        {
            isDragging = true;

            // Set the position to the mouse position
            transform.position = new Vector3(Camera.ScreenToWorldPoint(Input.mousePosition).x,
                                             Camera.ScreenToWorldPoint(Input.mousePosition).y,
                                             mousePosition3D.z);
        }
        else
        {
            isDragging = false;
        }
    }

}