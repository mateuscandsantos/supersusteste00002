﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DragAndDropUIObjectBehaviour : MonoBehaviour, IDragHandler
{
    public Camera Camera;

    public void OnDrag(PointerEventData eventData)
    {

        if (!MiniGameProgressController.isMiniGamePaused)
        {
            var screenPoint = new Vector3(Input.mousePosition.x,
                                      Input.mousePosition.y,
                                      Input.mousePosition.z);

            screenPoint.z = 10.0f;

            transform.position = Camera.ScreenToWorldPoint(screenPoint);
        }
    }

    public void OnEndDrag(PointerEventData eventData) {

        if (!MiniGameProgressController.isMiniGamePaused)
        {
            transform.localPosition = Vector3.zero;
        }
    }

}