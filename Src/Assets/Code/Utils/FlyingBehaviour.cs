﻿using UnityEngine;

public class FlyingBehaviour : MonoBehaviour
{

    public float speed = 0.001f;
    public float rotateSpeed = 0.8f;

    public float initialX;
    public float initialY;
    public float initialZ;
    public float delta = 0.3f;

    Vector3 newPosition;

    public static bool killMosquitos = false;

    void Start()
    {
        GetComponent<Rigidbody2D>().gravityScale = 0f;
        initialX = transform.position.x;
        initialY = transform.position.y;
        initialZ = transform.position.z;

        PositionChange();
    }

    void PositionChange()
    {
        newPosition = new Vector3(Random.Range(initialX - delta, initialX + delta), 
                                  Random.Range(initialY - delta, initialY + delta),
                                               initialZ);
    }

    void Update()
    {

        if (killMosquitos) {
            GetComponent<Rigidbody2D>().gravityScale = 1f;
        } else {
            if (Vector2.Distance(transform.position, newPosition) < 1)
                PositionChange();

            transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * speed);
            transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * speed);

            LookAt2D(newPosition);
        }
    }

    void LookAt2D(Vector3 lookAtPosition)
    {
        float distanceX = lookAtPosition.x - transform.position.x;
        float distanceY = lookAtPosition.y - transform.position.y;
        float angle = Mathf.Atan2(distanceX, distanceY) * Mathf.Rad2Deg;

        Quaternion endRotation = Quaternion.AngleAxis(angle, Vector3.back);
        transform.rotation = Quaternion.Slerp(transform.rotation, endRotation, Time.deltaTime * rotateSpeed);
    }

}