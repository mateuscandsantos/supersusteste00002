﻿using UnityEngine;

public class GameObjectsUtils
{

    public bool ClickedGameObject(Camera camera, GameObject obj)
    {

        if (Input.GetButton("Fire1") && obj != null)
        {

            if (obj.GetComponent<SpriteRenderer>() != null)
            {
                // Gets the world position of the mouse on the screen        
                Vector2 mousePosition2D = camera.ScreenToWorldPoint(Input.mousePosition);
                Vector3 mousePosition3D = new Vector3(mousePosition2D.x,
                                                      mousePosition2D.y,
                                                      obj.GetComponent<SpriteRenderer>().bounds.center.z);

                // Checks whether the mouse is over the sprite 3D area and if the user 
                // is pressing the mouse button / touching the screen
                if (obj.GetComponent<SpriteRenderer>().bounds.Contains(mousePosition3D))
                {
                    return true;
                }
            }
        }

        return false;
    }

    /*
     * Ensure that the coordinates of an object A are on the same Z value of an
     * object B and then verify if the object a is inside of the object B.
     */
    public bool IsGameObjectAInsideB(Vector3 posObjA3d, GameObject objB)
    {
        Vector3 posObjA2d = new Vector3(posObjA3d.x,
                                        posObjA3d.y,
                                        objB.GetComponent<SpriteRenderer>().bounds.center.z);

        if (objB.GetComponent<SpriteRenderer>().bounds.Contains(posObjA2d))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void UpdateGameObjectX(GameObject gameObj, float newX)
    {
        gameObj.transform.position = new Vector3(newX,
                                                 gameObj.transform.position.y,
                                                 gameObj.transform.position.z);
    }

    public void UpdateGameObjectY(GameObject gameObj, float newY)
    {
        gameObj.transform.position = new Vector3(gameObj.transform.position.x,
                                                 newY,
                                                 gameObj.transform.position.z);
    }

}