﻿using System;

public class MathUtils
{

    public static bool IsEqual(float a, float b)
    {
        return Math.Abs(a - b) <= float.Epsilon;
    }

    public static bool is2dGameObjectInsideAnother() {
        return false;
    }

}